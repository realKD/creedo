<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="container">
	<div class="parameterArea" entryId="xarfparams"
		componentId="${componentId}" parameterUpdateActionId="${xarfparamsUpdateActionId}"></div>
	<hr />
	<div class="parameterArea" entryId="otheroptions"
		componentId="${componentId}" parameterUpdateActionId="${otheroptionsUpdateActionId}"></div>
	<button class="btn btn-default actionLink" frameId="${componentId}"
		linkId="${openActionId}" resultTarget="POPUP">Open</button>
</div>
