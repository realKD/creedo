/**
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @version 0.6.0
 * 
 */
CREEDO.namespace("CREEDO.dataviewcontainer");

CREEDO.dataviewcontainer = (function() {

	console.log("loading module: CREEDO.dataviewcontainer");

	var creedoCore = CREEDO.core;

	var dataViewContainerId = $("#creedo-dataviewcontainer")
			.attr("componentId");

	var updatePointCloudActionId = $("#creedo-dataviewcontainer").attr(
			"updatePointCloudActionId");

	var mainTable = $("#content-table .datatable");
	var fixedColumnsTable;
	var metadataTable = $("#content-metadata .datatable");

	var viewFilter = {};
	
	const plot = {
		data : null,
		axesTitles : null,
		invalid : true,
		
		draw : function() {
			const self=this;
			if (!this.visible()) {
				console.log("scatter plot invisible: invalidate and exit")
				this.invalid = true;
				return;
			}
			var $container = $('#content-point-cloud');
			if ($container.length == 0) {
				return;
			}
			$container.height(calcTableHeight());

			if (this.axesTitles == null) {
				CREEDO.core.requestData(dataViewContainerId, "pointAxesTitles",
						function(axesTitles) {
							self.axesTitles = axesTitles;
							self.draw();
						});
				return;
			} 
			
			if (this.data == null) {
				CREEDO.core.requestData(dataViewContainerId, "pointData",
						function(data) {
							self.data = data;
							self.draw();
						});
				return;
			}
					
			$container.highcharts({
									chart : {
										type : 'scatter',
										zoomType : 'xy',
										events : {
											redraw : function(){ this.creedoSymbolLegend.update(); },
											load : function() {
												creedoSymbolLegend(this, 25, 5);
												$('.highcharts-axis-title')
														.tooltip(
																{
																	container : "body",
																	placement : "bottom",
																	title : "click to change",
																	trigger : "hover",
																	html : "true"
																});

												$('.highcharts-legend-item')
														.tooltip(
																{
																	container : "body",
																	placement : "bottom",
																	title : "click to toogle visibility",
																	trigger : "hover",
																	html : "true",
																	show : "true"
																});
												$('.highcharts-axis-title').off('click')
													.on('click', function() {plot.openSetupDialog()});
											}
										}
									},
									title : {
										text : ''
									},
									xAxis : {
										title : {
											text : plot.axesTitles[0],
											style : {
												color : 'black',
												fontSize : '14px'
											}
										},
										labels : {
											style : {
												color : 'black',
												fontSize : '13px'
											}
										},
										lineColor : '#000000',
										tickColor : '#000000'
									},
									yAxis : {
										title : {
											text : plot.axesTitles[1],
											style : {
												color : 'black',
												fontSize : '14px'
											}
										},
										lineColor : '#000000',
										labels : {
											style : {
												color : 'black',
												fontSize : '13px'
											}
										},
										lineWidth : 1,
										gridLineWidth : 0,
										tickWidth : 1,
										tickColor : '#000000'
									},
									tooltip : {
										formatter : function() {
											return this.point.objName
													+ "<br>"
													+ (this.point.creedoCategoryName ? ("<b>category:</b> "
															+ this.point.creedoCategoryName + "<br>")
															: "") + "<b>x:</b> "
													+ this.point.x + "<br>"
													+ "<b>y:</b> " + this.point.y;
										}
									},
									series : generateSeries(),
									plotOptions : {
										series : {
											marker : {
												lineColor : 'rgba(20, 20, 20, .5)',
												lineWidth : 1,
												radius : 3,
												symbol : scatterCategorySymbols[0]
											},
											stickyTracking : false,
											dataLabels : {
												enabled : plot.data.length <= 10000,
												formatter : function() {
													return this.point.showLabel ? this.point.objName
															: null;
												},
												style : {
													fontSize : '13px'
												}
											},
											point : {
												events : {
													click : plot.data.length <= 10000 ? function(
															e) {
														if (!this.showLabel) {
															this.update({
																showLabel : true
															});
														} else {
															this.update({
																showLabel : false
															});
														}
													}
															: null
												}
											}
										}
									},
								});
				this.invalid = false;
		},
		
		openSetupDialog : function() {
			const self=this;
			console.log(this);
			var dialogContent = $('<div/>');

			bootbox.dialog({
				title : "Set visualization parameters",
				message : dialogContent,
				buttons : {
					success : {
						/**
						 * @required String this button's label
						 */
						label : "Draw",

						/**
						 * @optional String an additional class to apply to the
						 *           button
						 */
						className : "btn-success disabled creedo-redraw-button",

						callback : function() {
							self.axesTitles = null;
							self.data = null;
							self.draw();
						}
					},
				}
			});

			var redrawButton = $('.creedo-redraw-button');

			dialogContent.load = function() {
				creedoCore.requestData(dataViewContainerId, "pointParameters",
						function(params) {
							var allValid = CREEDO.parameters.renderParameters(
									dialogContent, params);
							if (allValid) {
								redrawButton.removeClass('disabled');
							} else {
								redrawButton.addClass('disabled');
							}
						});
			};

			dialogContent.on('creedo:param-changed', function(event, paramName,
					paramValue) {
				creedoCore.performAction(dataViewContainerId,
						updatePointCloudActionId, [ paramName, paramValue ],
						function() {
							dialogContent.load();
						});
			});

			dialogContent.load();
		},
		
		redrawSeries : function () {
			if (!this.visible()) {
				this.invalid = true;
				return;
			}
			let chart=$('#content-point-cloud').highcharts();
	        for (var i = chart.series.length-1; i>=0; i--) {
	            chart.series[i].remove(false);
	        }
			let newSeries=generateSeries();
			newSeries.forEach(function(series) {chart.addSeries(series,false);});
			chart.redraw();
			// here needs to set to valid in final state machine model
		},
		
		visible : function() {
			return $('#content-point-cloud').hasClass('active');
		}
	}

	/**
	 * Exported; Filter objects have string members "id", "name", and array
	 * member "content" with data ids passing the filter
	 */
	var addFilter = function(filter) {
		viewFilter[filter.id] = filter;
	};

	/**
	 * Exported;
	 */
	var removeFilter = function(id) {
		delete viewFilter[id];
	};

	function filterArray() {
		var result = [];
		for ( var id in viewFilter) {
			if (!viewFilter.hasOwnProperty(id)) {
				continue;
			}
			result.push(viewFilter[id]);
		}
		return result;
	}

	var datatableFilter = function(settings, data, index) {
		var filters;

		if (settings.oInstance.selector !== "#content-table .datatable") {
			// we apply filtering only to main table
			return true;
		}

		filters = filterArray();
		for (var i = 0; i < filters.length; i++) {
			if ($.inArray(index, filters[i].content) === -1) {
				return false;
			}
		}
		return true;
	};

	var scatterCategorySymbols = {
		0 : "circle",
		1 : "square",
		2 : "triangle",
		3 : "triangle-down",
		4 : "diamond",
		5 : "plus"
	};

	var categorySymbol = function(category) {
		return scatterCategorySymbols[category];
	}

	var processColumnTooltips = function(tooltips) {
		var $columns = $(".datatable-column");
		$.each($columns, function(index, column) {
			$(column).tooltip({
				container : "body",
				placement : "bottom",
				title : tooltips[index],
				trigger : "hover",
				html : "true"
			});
		});
	}

	var processDescription = function(description) {
		$("#datasetNameElement").tooltip({
			container : "body",
			placement : "bottom",
			title : description,
			trigger : "hover",
			html : "true"
		});
	}

	var processTableData = function(data) {
		mainTable.removeClass("hide").dataTable({
			"scrollX" : true,
			"scrollY" : calcTableHeight() + 'px',
			"scrollCollapse" : true,
			"bSortClasses" : false,
			"bLengthChange" : false,
			"iDisplayLength" : 40,
			"bScrollCollapse" : false,
			"fnDrawCallback" : function() {
				if (fixedColumnsTable != null) {
					fixedColumnsTable.fnRedrawLayout();
				}
			}
		});

		fixedColumnsTable = new $.fn.dataTable.FixedColumns(mainTable);

		/*
		 * Here an index is given to each row in the table The index corresponds
		 * to the index of the data received from the server side
		 */
		$.each(data, function(idx, row) {
			mainTable.fnAddData(row, false);
		});
		$("#content-table .loader").addClass("hide");
		// console.log("not calling: mainTable.fnDraw()");
		mainTable.fnDraw();
		fixedColumnsTable.fnUpdate();
		fixedColumnsTable.fnRedrawLayout();
	};
		
	function mean(iterable, property) {
		if (iterable.length<1) return NaN;
		let m=0;
		$.each(iterable, function(index,item) {
			m=m+item[property];
		});
		return m/iterable.length;
	}
	
	function confInt(iterable, property, delta) {
		const n=iterable.length;
		const denominator=(Math.pow(n,2)*delta-n);
		if (denominator<=0) return NaN;
		const m=mean(iterable, property);
		let s=0;
		$.each(iterable, function(index,item) {
			s=s+Math.pow(item[property]-m,2);
		});
		s=s/(n-1);
		return Math.sqrt((Math.pow(n,2)-1)*s/denominator);
// const n=iterable.length;
// if (n<=1) return NaN;
// const m=mean(iterable, property);
// let s=0;
// $.each(iterable, function(index,item) {
// s=s+Math.pow(item[property]-m,2);
// });
// return 1.645*Math.sqrt(s/(n*(n-1)));
	}
		
	const defaultPlotLineLabelStyle={
		align: 'right',
		x: -10,
		textAlign: 'right',
	};
	
	const unselectedPlotlineLabelStyle=Object.assign({},defaultPlotLineLabelStyle,{style : {color : 'grey'}});
	
	const defaultPlotlineStyle={
		color: 'black',
		dashStyle: 'solid',
		width: 1,
		label: defaultPlotLineLabelStyle,
	};
	
	const secondaryPlotlineStyle=Object.assign({},{
		dashStyle: 'shortDash',
	});

	const unselectedPlotlineStyle=Object.assign({},{
		color: 'grey',
		label: unselectedPlotlineLabelStyle,
	});
	
	function mimickingSeries(plotLineConfigs) {
		var first, all;
		if (Array.isArray(plotLineConfigs)) {
			if (plotLineConfigs.length==0) {
				return {};
			}
			first=plotLineConfigs[0];
			all=plotLineConfigs;
		}
		else {
			first=plotLineConfigs;
			all=[plotLineConfigs];
		}
			  return {
			    type: 'line',
			    name: first.id,
			    color: first.color,
			    visible : false,
			    dashStyle : first.dashStyle,
			    marker: {
		            enabled: false
		        },
			    events: {
			    		show : function() {
			        		all.forEach((config)=>{$('#content-point-cloud').highcharts().yAxis[0].addPlotLine(config);})				     
			    		},
			    		hide : function() {
			    			all.forEach((config)=>{$('#content-point-cloud').highcharts().yAxis[0].removePlotLine(config.id);});				    			
			    		},
			    		remove : function() {
			    			console.log('series remove event: '+first.id);
			    			all.forEach((config)=>{$('#content-point-cloud').highcharts().yAxis[0].removePlotLine(config.id);});				    						    			
			    		}
			    }
			  };
	}
	
	function plotLineConfig(id, vals) {
		var res=[],values;
		if (Array.isArray(vals)) {
			values=vals;
		}
		else {
			values=[vals];
		}
		for (let j=0; j<values.length; j++) {
			let v=values[j], config={};
			for (let i=2; i<arguments.length; i++) {
				let options=arguments[i];
				Object.assign(config,options);		
			}
			config.id=id;
			config.value=v;
			config.label=Object.assign({},config.label);
			config.label.text=v.toPrecision(3);
			res.push(config);
		}
		return res;
	}
	
	function generateSeries() {
		function seriesForTwoFilters(filter1, filter2) {
			var seriesAll = [];
			var seriesOnlyFirst = [];
			var seriesOnlySecond = [];
			var seriesNone = [];

// $.each(pointCloudData, function(index, item) {
			$.each(plot.data, function(index, item) {
				var inFirst = $.inArray(item.id, filter1.content) !== -1;
				var inSecond = $.inArray(item.id, filter2.content) !== -1;
				item.marker = {
					symbol : scatterCategorySymbols[item.category]
				};
				if (inFirst && inSecond) {
					seriesAll.push(item);
				} else if (inFirst) {
					seriesOnlyFirst.push(item);
				} else if (inSecond) {
					seriesOnlySecond.push(item);
				} else {
					seriesNone.push(item);
				}
			});

			var result = [];

			if (seriesNone.length > 0) {
				result.push({
					turboThreshold : 0,
					name : 'rest',
					color : 'rgba(155, 155, 155, .5)',
					data : seriesNone
				});
			}

			if (seriesOnlyFirst.length > 0) {
				result.push({
					turboThreshold : 0,
					name : seriesAll.length === 0 ? filter1.name : filter1.name
							+ " MINUS " + filter2.name,
					color : 'rgba(0, 0, 255, .5)',
					data : seriesOnlyFirst
				});
			}

			if (seriesOnlySecond.length > 0) {
				result.push({
					turboThreshold : 0,
					name : seriesAll.length === 0 ? filter2.name : filter2.name
							+ " MINUS " + filter1.name,
					color : seriesOnlyFirst.length > 0 ? 'rgba(0, 255,0, .5)'
							: 'rgba(0, 0, 255, .5)',
					data : seriesOnlySecond
				});
			}

			if (seriesAll.length > 0) {
				result.push({
					turboThreshold : 0,
					name : filter1.name + " AND " + filter2.name,
					color : 'rgba(255, 0, 0, .5)',
					data : seriesAll
				});
			}
			
// let m0=mean(pointCloudData, 'y');
			let m0=mean(plot.data, 'y');
// let epsilon0=confInt(pointCloudData, 'y', 0.1);
			let epsilon0=confInt(plot.data, 'y', 0.1);
			let m1=mean(seriesAll, 'y');
			let epsilon1=confInt(seriesAll, 'y', 0.1);
			
			result.push(mimickingSeries(plotLineConfig('local mean', m1, defaultPlotlineStyle)));
			result.push(mimickingSeries(plotLineConfig('global mean', m0, defaultPlotlineStyle, unselectedPlotlineStyle)));
			result.push(	mimickingSeries(plotLineConfig('local confidence bounds', m1+epsilon1, defaultPlotlineStyle, secondaryPlotlineStyle)));
			result.push(mimickingSeries(plotLineConfig('global confidence bounds', m0+epsilon0, defaultPlotlineStyle, secondaryPlotlineStyle, unselectedPlotlineStyle)));
			
			return result;
		}
				
		function seriesForOneFilter(filter) {
			var rest = [];
			var selected = [];
			$.each(plot.data, function(index, item) {
				item.marker = {
					symbol : scatterCategorySymbols[item.category]
				};
				if ($.inArray(item.id, filter.content) === -1) {
					rest.push(item);
				} else {
					selected.push(item);
				}
			});
			
			let m0=mean(plot.data, 'y');
			let epsilon0=confInt(plot.data, 'y', 0.1);
			let m1=mean(selected, 'y');
			let epsilon1=confInt(selected, 'y', 0.1);
			
			console.log(epsilon1);
			
			return [ {
				turboThreshold : 0,
				name : 'rest',
				color : 'rgba(155, 155, 155, .5)',
				data : rest
			}, 
			{
				turboThreshold : 0,
				name : filter.name,
				color : 'rgba(255, 0, 0, .5)',
				data : selected
			}, 
			mimickingSeries(plotLineConfig('global mean', m0, defaultPlotlineStyle, unselectedPlotlineStyle)),
			mimickingSeries(plotLineConfig('local mean', m1, defaultPlotlineStyle)),
			mimickingSeries(plotLineConfig('local confidence bounds', [m1-epsilon1, m1+epsilon1], defaultPlotlineStyle, secondaryPlotlineStyle)),
			mimickingSeries(plotLineConfig('global confidence bounds', [m0-epsilon0, m0+epsilon0], defaultPlotlineStyle, secondaryPlotlineStyle, unselectedPlotlineStyle))];
		}
		
		function seriesForNoFilter() {
			$.each(plot.data, function(index, item) {
				item.marker = {
					symbol : scatterCategorySymbols[item.category]
				};
			});
			
			let m0=mean(plot.data, 'y');
			let epsilon=confInt(plot.data, 'y', 0.1);
												
			console.log(m0);
			console.log(epsilon);
			
			return [ {
				turboThreshold : 0,
				name : 'global',
				color : 'rgba(255, 0, 0, .5)',
				data : plot.data
				},
				mimickingSeries(plotLineConfig('global mean', m0, defaultPlotlineStyle)),
				mimickingSeries(plotLineConfig('confidence interval', [m0-epsilon, m0+epsilon], defaultPlotlineStyle, secondaryPlotlineStyle))
			];
		}
		
		function seriesForArbitraryNumberOfFilters(filters) {
			function numberOfSatisfiedFilters(index) {
				var result = 0;
				for (var i = 0; i < filters.length; i++) {
					if ($.inArray(index, filters[i].content) !== -1) {
						result = result + 1;
					}
				}
				return result;
			}

			var seriesAll = [];
			var seriesSome = [];
			var seriesNone = [];

			$.each(plot.data, function(index, item) {
				item.marker = {
					symbol : scatterCategorySymbols[item.category]
				};
				var numberOfFilters = numberOfSatisfiedFilters(index);
				if (numberOfFilters === 0) {
					seriesNone.push(item);
				} else if (numberOfFilters === filters.length) {
					seriesAll.push(item);
				} else {
					seriesSome.push(item);
				}
			});

			var result = [];

			if (seriesNone.length > 0) {
				result.push({
					turboThreshold : 0,
					name : 'rest',
					color : 'rgba(155, 155, 155, .5)',
					data : seriesNone
				});
			}

			if (seriesSome.length > 0) {
				result.push({
					turboThreshold : 0,
					name : 'selected by some',
					color : 'rgba(0, 0, 255, .5)',
					data : seriesSome
				});
			}

			if (seriesAll.length > 0) {
				result.push({
					turboThreshold : 0,
					name : 'selected by all',
					color : 'rgba(255, 0, 0, .5)',
					data : seriesAll
				});
			}
			
			let m0=mean(plot.data, 'y');
			let epsilon0=confInt(plot.data, 'y', 0.1);
			let m1=mean(seriesAll, 'y');
			let epsilon1=confInt(seriesAll, 'y', 0.1);
			

			result.push(mimickingSeries(plotLineConfig('local mean', m1, defaultPlotlineStyle)));
			result.push(mimickingSeries(plotLineConfig('global mean', m0, defaultPlotlineStyle, unselectedPlotlineStyle)));
			result.push(	mimickingSeries(plotLineConfig('local confidence bounds', m1+epsilon1, defaultPlotlineStyle, secondaryPlotlineStyle)));
			result.push(mimickingSeries(plotLineConfig('global confidence bounds', m0+epsilon0, defaultPlotlineStyle, secondaryPlotlineStyle, unselectedPlotlineStyle)));
			
			return result;
		}
		
		var filters = filterArray();
		if (filters.length == 0) {
			return seriesForNoFilter();
		} else if (filters.length == 1) {
			return seriesForOneFilter(filters[0]);
		} else if (filters.length == 2) {
			return seriesForTwoFilters(filters[0], filters[1]);
		} else {
			return seriesForArbitraryNumberOfFilters(filters);
		}
	}
	
	function creedoSymbolLegend(chart, xOffset, yOffset) {
		var symbolLegend,
			symbolCategories = {};

		$.each(plot.data, function(index, item) {
							var symbol = categorySymbol(item.category);
							if (!symbolCategories[symbol]) {
								symbolCategories[symbol] = new Set();
							}
							symbolCategories[symbol]
									.add(item.creedoCategoryName);
						});
		
		symbolLegend = chart.renderer.g().translate(
				chart.plotLeft + xOffset, chart.plotTop + yOffset
			).attr({zIndex:10}).on('click', function() {plot.openSetupDialog();}); 
		
		function symbolLegendEntry(symbol, text, yOffset) {
			var result = chart.renderer.g().translate(0,yOffset).attr({zIndex: 11}).add(symbolLegend);
			chart.renderer.symbol(symbol,
					0, 7,
					6, 6, {
						'stroke' : 'blue',
						'fill' : 'red',
						'stroke-width' : 1
					}).attr({
						stroke: 'rgba(0,0,0,1)',
						fill: 'rgba(255,255,255,0.5)'
					}).add(result);
			
			chart.renderer.text(text,12,14).attr({
				fontSize: 14
			}).add(result);
			return result;
		}
		
		let i=0;
		for (var symbol in symbolCategories) {
		    if (symbolCategories.hasOwnProperty(symbol)) {
		    		var categories = Array.from(symbolCategories[symbol]).join(', ');
				symbolLegendEntry(symbol,categories,i*16).add(symbolLegend);
				i++;
		    }
		}
		
        chart.creedoSymbolLegend = symbolLegend;
        symbolLegend.update = function() {
        		if (i>1) {
        			this.translate(chart.plotLeft+xOffset, chart.plotTop + yOffset);
        		}
        	}

        if (i>1) {
        	symbolLegend.add();
        box = symbolLegend.getBBox();
        chart.renderer.rect(box.x - 5, box.y - 5, box.width + 10, box.height + 10, 5)
            .attr({
            	fill: 'rgba(255,255,255,0.5)',
                stroke: 'black',
                'stroke-width': 1,
                zIndex: 10
            })
            .add(symbolLegend);
        }
        	              
        return symbolLegend;
    }
	
	// events: {'click' : function() {
	// console.log(this);
	// this.options.label.align='left';
	// this.options.label.textAlign='left';
	// this.options.label.x=10;
	// this.svgElem.destroy();
	// this.svgElem = undefined;
	// this.render();
	// }
	// }
			
	var redrawFilteringDependentViews = function() {
		mainTable.fnDraw();
		plot.redrawSeries();
	};
	
	var plusSymbol = function (x, y, w, h) {
	    return [
	        'M', x, y + (5 * h) / 8,
	        'L', x, y + (3 * h) / 8,
	        'L', x + (3 * w) / 8, y + (3 * h) / 8,
	        'L', x + (3 * w) / 8, y,
	        'L', x + (5 * w) / 8, y,
	        'L', x + (5 * w) / 8, y + (3 * h) / 8,
	        'L', x + w, y + (3 * h) / 8,
	        'L', x + w, y + (5 * h) / 8,
	        'L', x + (5 * w) / 8, y + (5 * h) / 8,
	        'L', x + (5 * w) / 8, y + h,
	        'L', x + (3 * w) / 8, y + h,
	        'L', x + (3 * w) / 8, y + (5 * h) / 8,
	        'L', x, y + (5 * h) / 8,
	        'z'
	    ];
	};
	
	const exportData=function() {
		var chart = $('#content-point-cloud').highcharts();
		var w = window.open();
		$(w.document.body).html(
				"<pre><code>"
						+ JSON
								.stringify(
										{
											"xData selected" : chart.series[1].xData,
											"yData selected" : chart.series[1].yData,
											"xData rest" : chart.series[1].xData,
											"yData rest" : chart.series[1].yData
										},
										null,
										'\t')
						+ "</code></pre>");
	};
	
	const highchartsGlobalOptions={
		chart : {
			plotBorderWidth : 0,
			plotBorderColor : 'black',
			ignoreHiddenSeries : false
		},
		legend : {
			enabled : true,
			reversed : true,
			itemStyle : {
				fontSize : '14px',
				fontWeight: 'normal'
			}
		},
		series : {
			stickyTracking : false
		},
		credits : {
			enabled : false
		},
		navigation : {
			buttonOptions : {
				verticalAlign : 'bottom'
			}
		},
		exporting : {
			sourceWidth : 560,
			sourceHeight : 420,
			scale : 2,
			chartOptions : {
				chart : {
					plotBorderWidth : 0,
					spacingLeft : 0,
					spacingBottom : 0,
					spacingTop : 5,
					spacingRight : 5
				},
				xAxis : {
					title : {
						margin : 3
					},
				},
				yAxis : {
					title : {
						margin : 5
					},
				},
				legend : {
					margin : 3
				},
			}
		}

	};

	$(document)
			.ready(
					function() {

						console.log("init module: CREEDO.dataviewcontainer");
						
						Highcharts.SVGRenderer.prototype.symbols.plus = plusSymbol;
						if (Highcharts.VMLRenderer) {
						    Highcharts.VMLRenderer.prototype.symbols.plus = Highcharts.SVGRenderer.prototype.symbols.plus;
						}

						plot.draw();

						$.fn.dataTable.ext.search.push(datatableFilter);

						// renderDatatable();
						creedoCore.requestData(dataViewContainerId,
								"tableData", processTableData);
						creedoCore.requestData(dataViewContainerId,
								"columnTooltips", processColumnTooltips);
						creedoCore.requestData(dataViewContainerId,
								"description", processDescription);

						Highcharts.setOptions(highchartsGlobalOptions);
						
						let exportMenuItems=Highcharts.getOptions().exporting.buttons.contextButton.menuItems;
						exportMenuItems.unshift({
									text : 'Change parameters',
									onclick : function() {plot.openSetupDialog();} 
								});
						exportMenuItems.unshift({
									text : 'Open data window',
									onclick : exportData
								});

						$('#tab-content').on('shown.bs.tab', function(e) {
							mainTable.fnDraw();
						});
						$('#tab-point-cloud').on('shown.bs.tab', function(e) {
							if (plot.invalid) {
								plot.draw();
							}
						});
						// render metadata table
						$('#tab-metadata').on('shown.bs.tab', function(e) {
							$target = $(e.target);
							if (!$target.data('tableRendered')) {

								metadataTable.dataTable({
									"scrollY" : calcTableHeight() + 'px',
									"scrollCollapse" : true,
									"bSortClasses" : false,
									"bLengthChange" : false,
									"bFilter" : false,
									"paging" : false,
									"bScrollCollapse" : false
								});
								$target.data('tableRendered', true);
							}
							metadataTable.fnDraw();
						});

					});

	return {
		mainTable : mainTable,
		redrawFilteringDependentViews : redrawFilteringDependentViews,
		drawPointCloud : function() {plot.draw();},
		addFilter : addFilter,
		removeFilter : removeFilter
	};

}());