/**
 * Functionality of show/hide button
 * 
 * @author Mario Boley
 * 
 * @version 0.6.0
 * 
 */

$(document).ready(function() {

	var PARAMETERS = CREEDO.parameters;

	$('.parameterArea').hide();
	$('.edit-entry-button').click(function() {
		$paramArea = $('#parameterArea_' + $(this).attr('entryIndex'));
		if ($paramArea.is(':visible')) {
			$paramArea.hide();
			$(this).html('Show');
		} else {
			$paramArea.each(PARAMETERS.initParameterArea);
			$paramArea.show();
			$(this).html('Hide');
		}
	});
});
