package de.unibonn.creedo.setup;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.logging.Logger;

/**
 * Retrieves content packages that are packed as Java resources.
 * 
 * @author Mario Boley
 * 
 * @since 0.4.0
 * 
 * @version 0.4.0
 * 
 */
public class ContentPackages {

	private static final Logger LOGGER = Logger.getLogger(ContentPackages.class.getName());

	private static final ClassLoader CLASS_LOADER = ServerPaths.class.getClassLoader();

	private static final String INIT_DB_SUBDIR = "/database";

	private static final String INIT_CONTENTFILES_SUBDIR = "/contentfiles";

	private static final String INIT_BASE_PATH = "init/";

	/**
	 * Finds a content package that comes packed with the Creedo distribution or
	 * some external jar.
	 * 
	 * @param key
	 *            the string key under which the package information is stored
	 * @return content package with specified key
	 * @throws IOException
	 *             if no valid package stored with key
	 */
	public static ContentPackage packedResourceBasedContentPackage(String key) throws IOException {
		Optional<Path> contentFilePath = resourcePathNameToPath(INIT_BASE_PATH + key + INIT_CONTENTFILES_SUBDIR);
		Optional<Path> dbInitScriptPath = resourcePathNameToPath(INIT_BASE_PATH + key + INIT_DB_SUBDIR);
		return new DefaultContentPackage(key, contentFilePath, dbInitScriptPath);
	}

	private static Optional<Path> resourcePathNameToPath(String pathName) throws IOException {
		URL dbScriptResourcePath = CLASS_LOADER.getResource(pathName);
		if (dbScriptResourcePath == null) {
			LOGGER.warning("Resource '" + pathName + "' not found or inadequate privileges");
			return Optional.empty();
		}
		URI uri;
		try {
			uri = dbScriptResourcePath.toURI();
		} catch (URISyntaxException e) {
			throw new IOException("Path '" + dbScriptResourcePath + "' could not be converted to URI");
		}
		return Optional.of(Paths.get(uri));
	}

	private static class DefaultContentPackage implements ContentPackage {

		private final String id;
		private final Optional<Path> contentFilePath;
		private final Optional<Path> dbInitScriptPath;

		private DefaultContentPackage(String id, Optional<Path> contentFilePath, Optional<Path> dbInitScriptPath) {
			this.id = id;
			this.contentFilePath = contentFilePath;
			this.dbInitScriptPath = dbInitScriptPath;
		}

		@Override
		public String id() {
			return id;
		}

		@Override
		public Optional<Path> pathToContentFiles() {
			return contentFilePath;
		}

		@Override
		public Optional<Path> pathToDbInitScripts() {
			return dbInitScriptPath;
		}

	}

}
