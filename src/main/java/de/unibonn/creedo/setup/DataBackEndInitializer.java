package de.unibonn.creedo.setup;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import org.apache.ibatis.jdbc.ScriptRunner;

/**
 * Provides method for initializing the underlying database schema using sql
 * scripts from the init ressource path (accessed via {@link ServerPaths}).
 * 
 * WARNING: usage of this will be converted to a non-static component similar to
 * CreedoInitializer.
 * 
 * @author Björn Jacobs
 * 
 * @since 0.0.1
 * 
 * @version 0.4.0
 * 
 */
public class DataBackEndInitializer {

	private static final Logger LOGGER = Logger.getLogger(DataBackEndInitializer.class.getName());

	/**
	 * Creates a connection to the database - directory - host - port - database
	 * - user - password - dropexisting
	 */
	public static void main(String[] args) {
		if (args.length < 7) {
			System.out.println(
					"Usage: [dir to sql files] [hostname] [port] [database name] [username] [password] [dropexisting true/false]");
			System.exit(-1);
		}

		// String directory = new File(args[0]).getAbsolutePath();
		String directory = args[0];
		String hostname = args[1];
		String port = args[2];
		String databaseName = args[3];
		String username = args[4];
		String password = args[5];
		Boolean dropExisting = Boolean.valueOf(args[6]);

		try {
			ClassLoader classLoader = DataBackEndInitializer.class.getClassLoader();
			File dir = new File(classLoader.getResource("database_init/" + directory).getPath());
			if (dropExisting) {
				createDB(hostname, port, databaseName, username, password);
			}
			runScripts(dir, hostname, port, databaseName, username, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void createDB(String host, String port, String database, String username, String password) {
		try {
			List<Reader> readers = new ArrayList<>();

			readers.add(new StringReader("DROP DATABASE IF EXISTS `" + database + "`;"));
			readers.add(new StringReader("CREATE DATABASE `" + database + "`;"));

			Connection conn = getConnection(host, port, username, password);

			ScriptRunner runner = new ScriptRunner(conn);
			runner.setLogWriter(null);
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			PrintWriter outWriter = new PrintWriter(outStream);

			runner.setErrorLogWriter(outWriter);
			runner.setCharacterSetName("UTF-8");
			int i = 0;
			for (Reader reader : readers) {
				runner.runScript(reader);
				LOGGER.fine("Executed SQL script " + i++);

				if (outStream.size() > 0) {
					throw new RuntimeException(
							"Error starts with: " + new String(Arrays.copyOfRange(outStream.toByteArray(), 0, 1000)));
				}
			}
			conn.close();

		} catch (Exception e) {
			StringBuilder builder = new StringBuilder();
			builder.append("Database creation failed with").append(" hostname=").append(host).append(" port=")
					.append(port).append(" database=").append(database).append(" username=").append(username)
					.append("\nMessage: ").append(e.getMessage());
			String msg = builder.toString();

			throw new RuntimeException(msg, e);
		}

	}

	public static void runScripts(File directory, String host, String port, String database, String username,
			String password) {

		List<File> sqlCreateScriptFiles = findSqlCreateScripts(directory);

		try {
			List<Reader> readers = new ArrayList<>();

			for (File file : sqlCreateScriptFiles) {
				readers.add(new FileReader(file));
				;
			}

			Connection conn = getConnection(host, port, username, password);

			ScriptRunner runner = new ScriptRunner(conn);
			runner.setLogWriter(null);
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			PrintWriter outWriter = new PrintWriter(outStream);

			runner.setErrorLogWriter(outWriter);
			runner.setCharacterSetName("UTF-8");
			runner.runScript(new StringReader("USE `" + database + "`;"));
			int i = 0;
			for (Reader reader : readers) {
				runner.runScript(reader);
				LOGGER.fine("Executed SQL script " + i++);

				if (outStream.size() > 0) {
					throw new RuntimeException(
							"Error starts with: " + new String(Arrays.copyOfRange(outStream.toByteArray(), 0, 1000)));
				}
			}
			conn.close();

		} catch (Exception e) {
			StringBuilder builder = new StringBuilder();
			builder.append("Initialization failed with dir=").append(directory).append(" hostname=").append(host)
					.append(" port=").append(port).append(" database=").append(database).append(" username=")
					.append(username).append("\nMessage: ").append(e.getMessage());
			String msg = builder.toString();

			throw new RuntimeException(msg, e);
		}

	}

	private static Connection getConnection(String host, String port, String username, String password)
			throws ClassNotFoundException, IllegalAccessException, InstantiationException, SQLException {
		String connectionString = "jdbc:mysql://" + host + ":" + port + "/";
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		return DriverManager.getConnection(connectionString, username, password);
	}

	private static List<File> findSqlCreateScripts(File dir) {
		// File dir = new File(databaseFilesDir);

		if (!dir.isDirectory()) {
			throw new IllegalArgumentException(dir.getAbsolutePath() + " is no directory!");
		}
		File[] files = dir.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return !pathname.isDirectory() && pathname.getName().toLowerCase().endsWith(".sql");
			}
		});
		List<File> filesList = Arrays.asList(files);
		Collections.sort(filesList);
		return filesList;
	}

}
