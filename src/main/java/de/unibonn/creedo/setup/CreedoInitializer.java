package de.unibonn.creedo.setup;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import de.unibonn.creedo.ConfigurationProperties;
import de.unibonn.creedo.utils.SimpleCopyFileVisitor;

/**
 * <p>
 * Initializes application on instance creation, which is supposed to happen on
 * application start-up. At the moment this is achieved through Spring.
 * </p>
 * <p>
 * In particular, checks whether the data-backend is up and running. If not,
 * then the database schema AND the content folder are initialized from a
 * directory configured through the property file.
 * </p>
 * 
 * @author Mario Boley
 * 
 * @since 0.3.0
 * 
 * @version 0.4.0
 *
 */
public class CreedoInitializer {

	private static final Logger LOGGER = Logger.getLogger(CreedoInitializer.class.getName());

	private final DataBackEnd connectionFactory;

	private final ConfigurationProperties config;

	public CreedoInitializer(ConfigurationProperties properties, DataBackEnd connectionFactory) {
		checkNotNull(properties);
		checkNotNull(connectionFactory);
		this.config = properties;
		this.connectionFactory = connectionFactory;
		initSequence();
	}

	private void initDatabaseContent(Path sourceFolder) {
		LOGGER.warning("Init database content using ressource paths " + sourceFolder);
		DataBackEndInitializer.runScripts(sourceFolder.toFile(), config.CREEDO_DB_HOST, config.CREEDO_DB_PORT,
				config.CREEDO_DB_SCHEMA, config.CREEDO_DB_USER, config.CREEDO_DB_PASS);
	}

	private void initFileContent(Path sourcePath) throws RuntimeException {
		LOGGER.warning("Copy files to content folder " + ServerPaths.ABS_PATH_TO_CONFIGURED_RESSOURCE_DIR
				+ " from ressource path " + sourcePath + " (overwriting existing)");
		try {
			Files.walkFileTree(sourcePath, new SimpleCopyFileVisitor(ServerPaths.ABS_PATH_TO_CONFIGURED_RESSOURCE_DIR,
					StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES));
		} catch (IOException e) {
			throw new RuntimeException("Could not access content directory");
		}
	}

	private void initSequence() {
		LOGGER.info("Checking database backeend...");
		if (connectionFactory.online()) {
			LOGGER.info("Schema exists.");
		} else {
			LOGGER.severe("Schema does not exist or cannot be accessed.");
			LOGGER.warning("Creating schema.");
			DataBackEndInitializer.createDB(config.CREEDO_DB_HOST, config.CREEDO_DB_PORT, config.CREEDO_DB_SCHEMA,
					config.CREEDO_DB_USER, config.CREEDO_DB_PASS);

			configuredContentPackages().forEach(cp -> {
				LOGGER.warning("Installing package: " + cp.id());
				cp.pathToContentFiles().ifPresent(p->initFileContent(p));
				cp.pathToDbInitScripts().ifPresent(p->initDatabaseContent(p));
			});
		}
		LOGGER.info("Creedo is up and running...");
	}

	private List<ContentPackage> configuredContentPackages() {
		List<ContentPackage> result = new ArrayList<>();
		for (String key : config.CREEDO_INIT_CONTENT_KEYS) {
			try {
				result.add(ContentPackages.packedResourceBasedContentPackage(key));
			} catch (IOException e) {
				LOGGER.severe("Could not find well-formed content package '" + key + "' in packed resources; reason: "
						+ e.getMessage());
			}
		}
		return result;
	}

}
