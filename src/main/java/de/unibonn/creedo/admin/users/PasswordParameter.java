package de.unibonn.creedo.admin.users;

import static de.unibonn.realkd.common.base.Identifier.identifier;

import de.unibonn.creedo.common.BCrypt;
import de.unibonn.creedo.common.parameters.SecureParameter;
import de.unibonn.realkd.common.parameter.DefaultParameter;

public class PasswordParameter extends DefaultParameter<String> implements SecureParameter<String> {

	public PasswordParameter() {
		super(identifier("Password"),"Password", "Password used by user to log into Creedo", String.class,
				BCrypt.hashpw("", BCrypt.gensalt(12)), input -> input, x -> true, "");
	}

	@Override
	public void setByClearValue(String clearValue) {
		System.out.println("Hash raw value: " + clearValue);
		set(BCrypt.hashpw(clearValue, BCrypt.gensalt(12)));
	}

}