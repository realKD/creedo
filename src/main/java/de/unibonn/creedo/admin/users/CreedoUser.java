package de.unibonn.creedo.admin.users;

import static de.unibonn.creedo.admin.users.DefaultUserGroup.ANONYMOUS;

import java.util.Set;

public interface CreedoUser {

	public String id();

	public Set<UserGroup> groups();

	public String hashedPassword();
	
	public boolean active();
	
	public default boolean anonymous() {
		return groups().contains(ANONYMOUS);
	}

}
