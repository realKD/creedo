package de.unibonn.creedo.admin.users;

import static de.unibonn.realkd.common.parameter.Parameters.rangeEnumerableParameter;
import static de.unibonn.realkd.common.parameter.Parameters.subSetParameter;
import static de.unibonn.realkd.common.base.Identifier.id;

import java.util.List;
import java.util.Set;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import de.unibonn.creedo.common.parameters.SecureParameter;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;

public class DefaultUserDetails implements ParameterContainer, UserDetails {

	private static final ImmutableList<Boolean> BOOLS = ImmutableList.of(Boolean.FALSE, Boolean.TRUE);

	private final SecureParameter<String> password;

	private final SubCollectionParameter<UserGroup, Set<UserGroup>> optionalGroups;

	private final Parameter<Boolean> active;

	public DefaultUserDetails() {
		this.password = new PasswordParameter();
		this.optionalGroups = subSetParameter(id("Groups"), "Groups",
				"The optional groups this user belongs to (user will always be member of REGISTERED).",
				() -> UserGroups.get().optional());
		this.active = rangeEnumerableParameter(id("Active"),"Active",
				"Activation status of account (inactive users cannot log in)", Boolean.class, () -> BOOLS);
	}

	public DefaultUserDetails(String rawPassword, Set<UserGroup> userGroups) {
		this();
		password.setByClearValue(rawPassword);
		optionalGroups.set(userGroups);
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return ImmutableList.of(active, password, optionalGroups);
	}

	@Override
	public Set<UserGroup> groups() {
		return Sets.union(ImmutableSet.of(DefaultUserGroup.REGISTERED), optionalGroups.current());
	}

	@Override
	public String hashedPassword() {
		return password.current();
	}

	@Override
	public boolean active() {
		return active.current();
	}

}
