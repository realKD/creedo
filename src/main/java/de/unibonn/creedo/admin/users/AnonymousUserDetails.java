package de.unibonn.creedo.admin.users;

import static de.unibonn.realkd.common.base.Identifier.id;
import static de.unibonn.realkd.common.parameter.Parameters.subSetParameter;

import java.util.List;
import java.util.Set;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import de.unibonn.creedo.common.BCrypt;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;

public class AnonymousUserDetails implements UserDetails, ParameterContainer {

	private final SubCollectionParameter<UserGroup, Set<UserGroup>> optionalGroups;

	public AnonymousUserDetails() {
		this.optionalGroups = subSetParameter(id("Groups"), "Groups",
				"The optional groups the default user belongs to (will always be member of ANONYMOUS).",
				() -> UserGroups.get().optional());
	}

	@Override
	public Set<UserGroup> groups() {
		return Sets.union(ImmutableSet.of(DefaultUserGroup.ANONYMOUS), optionalGroups.current());
	}

	@Override
	public String hashedPassword() {
		return BCrypt.hashpw("", BCrypt.gensalt(12));
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return ImmutableList.of(optionalGroups);
	}

}
