/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.creedo.boot;

import static de.unibonn.realkd.common.parameter.Parameters.subListParameter;
import static de.unibonn.realkd.common.base.Identifier.id;
import static de.unibonn.realkd.common.workspace.Workspaces.workspace;
import static java.util.stream.Collectors.toList;

import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.logging.Logger;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.IdentifierInRepositoryParameter;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.creedo.setup.ServerPaths;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;
import de.unibonn.realkd.common.base.ValidationException;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.PropositionalContextFromTableBuilder;
import de.unibonn.realkd.data.table.AttributesFromGroupMapper;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.XarfImport;

/**
 * Reads data table from cvs file plus metadata file and adds propositional
 * logic with pre-configured mappers to workspace.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.1
 * 
 * @version 0.5.0
 *
 */
public class XarfBasedWorkspaceBuilder implements ParameterContainer, WorkspaceBuilder {

	public static final Logger LOGGER = Logger.getLogger(XarfBasedWorkspaceBuilder.class.getName());

	private final Parameter<String> dataFileIdentifier;

	private final PropositionalContextFromTableBuilder propLogicFactory;

	private final SubCollectionParameter<String, List<String>> additionalEntityFiles;

	private final SubCollectionParameter<AttributesFromGroupMapper, List<AttributesFromGroupMapper>> groupMappers;

	private final List<Parameter<?>> parameters;

	private Optional<Path> backupPath = Optional.empty();

	public XarfBasedWorkspaceBuilder() {
		this.propLogicFactory = new PropositionalContextFromTableBuilder();
		this.dataFileIdentifier = new IdentifierInRepositoryParameter<>(id("Data_file"), "Data file",
				"File with main data content given in the form of one row per data entity of delimeter-separated values.",
				ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
				Repositories.getIdIsFilenameWithExtensionPredicate(Repositories.ARF_FILE_EXTENSIONS));
		this.groupMappers = subListParameter(id("Group_mappers"), "Group mappers",
				"Mappers that are used to create derived attributes from the attribute groups.",
				() -> ImmutableList.copyOf(AttributesFromGroupMapper.values()), l -> true,
				() -> ImmutableList.copyOf(AttributesFromGroupMapper.values()));
		Supplier<List<String>> allEntityFiles = () -> ApplicationRepositories.CONTENT_FOLDER_REPOSITORY.getAllEntries()
				.stream()
				.filter(Repositories.getIdIsFilenameWithExtensionPredicate(Repositories.REALKD_ENTITY_FILE_EXTENSIONS))
				.map(e -> e.getId()).collect(toList());
		this.additionalEntityFiles = subListParameter(id("Additional_entities"), "Additional entities",
				"Additional entity files to be deserialized after all data entities from files above have been build",
				allEntityFiles);

		this.parameters = ImmutableList.of(dataFileIdentifier, groupMappers,
				propLogicFactory.attributeMapperParameter(), additionalEntityFiles);
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return parameters;
	}

	@Override
	public XarfBasedWorkspaceBuilder backupPath(Path path) {
		this.backupPath = Optional.ofNullable(path);
		return this;
	}

	@Override
	public Workspace get() throws ValidationException {
		validate();
		LOGGER.fine("Building workspace");
		XarfImport tableBuilder = XarfImport.xarfImport();
		tableBuilder.filename(
				ServerPaths.ABS_PATH_TO_CONFIGURED_RESSOURCE_DIR.resolve(dataFileIdentifier.current()).toString());
		tableBuilder.groupMappers(groupMappers.current());

		Workspace result = backupPath.isPresent() ? workspace(backupPath.get()) : workspace();
		DataTable table = tableBuilder.get();
		result.add(table.population());
		result.add(table);
		result.add(propLogicFactory.build(table));
		result.deserializeAll(ServerPaths.readAdditionalEntityFiles(additionalEntityFiles.current()));

		return result;
	}
}
