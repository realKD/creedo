/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.creedo.boot;

import static de.unibonn.creedo.setup.ServerPaths.contentFileContentAsString;
import static de.unibonn.realkd.common.parameter.Parameters.stringParameter;
import static de.unibonn.realkd.common.base.Identifier.id;
import static de.unibonn.realkd.common.workspace.Workspaces.workspace;
import static java.util.stream.Collectors.toList;

import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.logging.Logger;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.IdentifierInRepositoryParameter;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.creedo.setup.ServerPaths;
import de.unibonn.realkd.common.base.ValidationException;
import de.unibonn.realkd.common.parameter.DefaultParameter;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.Parameters;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.PropositionalContextFromTableBuilder;
import de.unibonn.realkd.data.table.AttributesFromGroupMapper;
import de.unibonn.realkd.data.table.DataFormatException;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.DataTableFromCSVBuilder;

/**
 * Reads data table from cvs file plus metadata file and adds propositional
 * logic with pre-configured mappers to workspace.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.1
 * 
 * @version 0.5.0
 *
 */
public class FixedComponentWorkspaceBuilder implements ParameterContainer, WorkspaceBuilder {

	public static final Logger LOGGER = Logger.getLogger(FixedComponentWorkspaceBuilder.class.getName());

	private final Parameter<String> dataFileIdentifier;

	private final Parameter<String> attributeFileIdentifier;

	private final Parameter<String> attributeGroupFileIdentifier;

	private final Parameter<Character> delimeter;

	private final Parameter<String> missingCode;

	private final Parameter<String> tableName;

	private final Parameter<String> tableDescription;

	private final List<Parameter<?>> parameters;

	private final PropositionalContextFromTableBuilder propLogicFactory;

	private final SubCollectionParameter<String, List<String>> additionalEntityFiles;

	private final SubCollectionParameter<AttributesFromGroupMapper, List<AttributesFromGroupMapper>> groupMappers;

	private Optional<Path> backupPath = Optional.empty();

	public FixedComponentWorkspaceBuilder() {
		this.propLogicFactory = new PropositionalContextFromTableBuilder();
		this.tableName = stringParameter(id("Table_name"), "Table name", "Name of the datatable.", "?",
				value -> value != null && value.length() > 0, "Enter non-empty string.");

		this.tableDescription = stringParameter(id("Table_description"), "Table description",
				"Description of the datatable.", "?", value -> value != null, "Enter description.");

		this.dataFileIdentifier = new IdentifierInRepositoryParameter<>(id("Data_file"), "Data file",
				"File with main data content given in the form of one row per data entity of delimeter-separated values.",
				ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
				Repositories.getIdIsFilenameWithExtensionPredicate(Repositories.CSV_FILE_EXTENSIONS));
		this.attributeFileIdentifier = new IdentifierInRepositoryParameter<>(id("Attributes_file"), "Attributes file",
				"File with single attribute meta data given in the form of one row per attribute in the form of 3 delimeter-separated values corresponding to name, type, and description, respectively.",
				ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
				Repositories.getIdIsFilenameWithExtensionPredicate(Repositories.CSV_FILE_EXTENSIONS));

		this.attributeGroupFileIdentifier = IdentifierInRepositoryParameter.getOptionalIdentifierInRepositoryParameter(
				id("Groups_file"), "Groups file", "File with attribute group meta data content.",
				ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
				Repositories.getIdIsFilenameWithExtensionPredicate(Repositories.CSV_FILE_EXTENSIONS));

		this.delimeter = new DefaultParameter<Character>(id("Delimeter"), "Delimeter",
				"Delimeter used in csv-files to seperate entries (default is ';').", Character.class, ';',
				string -> string.length() == 1 ? string.charAt(0) : ';', value -> value != null, "Enter single symbol");

		this.missingCode = stringParameter(id("Missing_code"), "Missing code",
				"String to indicate missing value in raw data.", "?", value -> value != null, "Enter string.");

		this.groupMappers = Parameters.subListParameter(id("Group_mappers"), "Group mappers",
				"Mappers that are used to create derived attributes from the attribute groups.",
				() -> ImmutableList.copyOf(AttributesFromGroupMapper.values()), l -> true,
				() -> ImmutableList.copyOf(AttributesFromGroupMapper.values()));

		Supplier<List<String>> allEntityFiles = () -> ApplicationRepositories.CONTENT_FOLDER_REPOSITORY.getAllEntries()
				.stream()
				.filter(Repositories.getIdIsFilenameWithExtensionPredicate(Repositories.REALKD_ENTITY_FILE_EXTENSIONS))
				.map(e -> e.getId()).collect(toList());
		this.additionalEntityFiles = Parameters.subListParameter(id("Additional_entities"), "Additional entities",
				"Additional entity files to be deserialized after all data entities from files above have been build",
				allEntityFiles);

		this.parameters = ImmutableList.of(tableName, tableDescription, dataFileIdentifier, attributeFileIdentifier,
				attributeGroupFileIdentifier, delimeter, missingCode, groupMappers,
				propLogicFactory.attributeMapperParameter(), additionalEntityFiles);
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return parameters;
	}

	@Override
	public FixedComponentWorkspaceBuilder backupPath(Path path) {
		this.backupPath = Optional.ofNullable(path);
		return this;
	}

	@Override
	public Workspace get() throws ValidationException {
		validate();
		LOGGER.fine("Building workspace");
		DataTableFromCSVBuilder tableBuilder = new DataTableFromCSVBuilder();
		tableBuilder.id(tableName.current());
		tableBuilder.description(tableDescription.current());
		tableBuilder.dataCSV(contentFileContentAsString(dataFileIdentifier.current()));
		tableBuilder.attributeMetadataCSV(contentFileContentAsString(attributeFileIdentifier.current()));
		tableBuilder.attributeGroupCSV(attributeGroupFileIdentifier.current().equals("---") ? ""
				: contentFileContentAsString(attributeGroupFileIdentifier.current()));
		tableBuilder.delimiter(delimeter.current());
		tableBuilder.missingSymbol(missingCode.current());
		tableBuilder.groupMappers(groupMappers.current());

		Workspace result = backupPath.isPresent() ? workspace(backupPath.get()) : workspace();
		DataTable table;
		try {
			table = tableBuilder.build();
			result.add(table.population());
			result.add(table);
		} catch (DataFormatException e) {
			throw new IllegalArgumentException(e);
		}

		result.add(propLogicFactory.build(table));
		result.deserializeAll(ServerPaths.readAdditionalEntityFiles(additionalEntityFiles.current()));

		return result;
	}
}
