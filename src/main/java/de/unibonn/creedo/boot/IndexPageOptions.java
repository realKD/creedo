package de.unibonn.creedo.boot;

import static de.unibonn.creedo.repositories.IdentifierInRepositoryParameter.optionalRepositoryIdParameter;
import static de.unibonn.realkd.common.parameter.Parameters.stringParameter;
import static de.unibonn.realkd.common.base.Identifier.id;

import java.util.List;
import java.util.Optional;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.util.Predicates;

public class IndexPageOptions implements ParameterContainer {

	private final Parameter<String> title;
	private final Parameter<String> referenceName;
	private final Parameter<Optional<String>> contentPageFileName;
	private final Parameter<String> loginInviteParameter;

	private final List<Parameter<?>> parameters;

	public IndexPageOptions() {
		title = stringParameter(id("Title"), "Title", "The title of the index page, e.g., \"Home\"", "Home",
				Predicates.satisfied(), "");

		referenceName = stringParameter(id("Reference_name"), "Reference name",
				"A short name for referencing the index page, e.g., from the navbar.", "Home", Predicates.satisfied(),
				"");

		contentPageFileName = optionalRepositoryIdParameter(id("Content_file_name"), "Content file name",
				"The name of the html file containing the main content of the index page.",
				ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
				Repositories.getIdIsFilenameWithExtensionPredicate(Repositories.HTML_FILE_EXTENSIONS));

		loginInviteParameter = stringParameter(id("Login_invitation"), "Login invitation",
				"An invitation text that is diplayed below the maincontent to users who are not logged into the application with their accoint (should contain a link to open the login page).",
				"", Predicates.notNull(), "");

		parameters = ImmutableList.of(title, referenceName, contentPageFileName, loginInviteParameter);
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return parameters;
	}

	public String title() {
		return title.current();
	}

	public String referenceName() {
		return referenceName.current();
	}

	public Optional<String> contentPageFilename() {
		return contentPageFileName.current();
	}

	public String loginInvite() {
		return loginInviteParameter.current();
	}

}
