/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2017 The Contributors of the Creedo Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.creedo.boot;

import java.nio.file.Path;

import de.unibonn.realkd.common.RuntimeSupplier;
import de.unibonn.realkd.common.base.ValidationException;
import de.unibonn.realkd.common.workspace.Workspace;

/**
 * Provides workspaces that optionally are persistent in the file system if
 * backup-path is supplied.
 * 
 * @author Mario Boley
 * 
 * @since 0.5.0
 * 
 * @version 0.5.0
 *
 */
public interface WorkspaceBuilder extends RuntimeSupplier<Workspace> {

	/**
	 * If provided constructs a file system based workspace that stores serialized
	 * versions of data artifacts in provided path.
	 * 
	 * @param path
	 *            the path for storing the workspace elements
	 * @return this builder
	 * 
	 */
	public WorkspaceBuilder backupPath(Path path);

	public Workspace get() throws ValidationException;

}