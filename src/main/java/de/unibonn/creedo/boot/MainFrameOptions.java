package de.unibonn.creedo.boot;

import static de.unibonn.creedo.repositories.Repositories.getIdentifierListOfRepositoryParameter;
import static de.unibonn.realkd.common.parameter.Parameters.booleanParameter;
import static de.unibonn.realkd.common.base.Identifier.id;

import java.util.List;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;

public class MainFrameOptions implements ParameterContainer {

	private final SubCollectionParameter<String, List<String>> optionalNavbarContents;

	private final SubCollectionParameter<String, List<String>> optionalFooterContents;

	private final Parameter<Boolean> showPageTitle;

	private final Parameter<Boolean> showNavbar;

	// private final Parameter<Boolean> showLoginLink;

	private final List<Parameter<?>> parameters;

	public MainFrameOptions() {
		this.optionalNavbarContents = getIdentifierListOfRepositoryParameter(id("Navbar_entries"), "Navbar entries",
				"List of optional content to be linked from the navbar of frame.",
				ApplicationRepositories.PAGE_REPOSITORY);
		this.optionalFooterContents = getIdentifierListOfRepositoryParameter(id("Footer_entries"), "Footer entries",
				"List of optional content to be linked from the footer of frame.",
				ApplicationRepositories.PAGE_REPOSITORY);
		this.showPageTitle = booleanParameter(id("Show_page_title"), "Show page title",
				"Whether page titles (and logos) are displayed on pages shown in the main frame.");
		this.showNavbar = booleanParameter(id("Show_navbar"), "Show navbar", "Whether main frame shows navbar.");
		// this.showLoginLink = Parameters.booleanParameter("Show login link",
		// "Whether a link to the login page should be added to the navbar for non
		// logged in users (anonymous default user).");
		this.parameters = ImmutableList.of(showNavbar, showPageTitle, optionalNavbarContents, optionalFooterContents);
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return parameters;
	}

	public List<String> optionalNavbarContents() {
		return optionalNavbarContents.current();
	}

	public List<String> optionalFooterContents() {
		return optionalFooterContents.current();
	}

	public boolean showPageTitle() {
		return showPageTitle.current();
	}

	public boolean showNavbar() {
		return showNavbar.current();
	}

	// public boolean showLoginLink() {
	// return showLoginLink.current();
	// }

}
