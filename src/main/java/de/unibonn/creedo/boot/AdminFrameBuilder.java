package de.unibonn.creedo.boot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.ConfigurationProperties;
import de.unibonn.creedo.admin.options.Options;
import de.unibonn.creedo.admin.users.DefaultUserDetails;
import de.unibonn.creedo.admin.users.UserDetails;
import de.unibonn.creedo.admin.users.Users;
import de.unibonn.creedo.studies.StudyBuilder;
import de.unibonn.creedo.studies.designs.EvaluationScheme;
import de.unibonn.creedo.studies.designs.StudyDesignBuilder;
import de.unibonn.creedo.studies.designs.SystemSpecBuilder;
import de.unibonn.creedo.studies.designs.TaskSpecBuilder;
import de.unibonn.creedo.studies.designs.TaskSpecification;
import de.unibonn.creedo.ui.ContentPageBuilder;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.Frame;
import de.unibonn.creedo.ui.core.LoadPageAction;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.ui.core.PageContainer;
import de.unibonn.creedo.ui.indexpage.DashboardLinkBuilder;
import de.unibonn.creedo.ui.repository.RepositoryAdministrationPage;
import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.creedo.webapp.dashboard.mining.MiningSystemBuilder;
import de.unibonn.realkd.common.RuntimeBuilder;
import de.unibonn.realkd.common.parameter.ParameterContainer;

public class AdminFrameBuilder implements RuntimeBuilder<Frame, CreedoSession> {

	@Override
	public Frame build(final CreedoSession creedoSession) {
		PageContainer adminFramePageContainer = new PageContainer(creedoSession.uiRegister().nextId());
		List<Action> adminFrameActions = new ArrayList<>();

		RepositoryAdministrationPage<ParameterContainer> optionManagementPage = new DefaultRepositoryAdministrationPageBuilder<ParameterContainer>()
				.setRepository(Options.getRepository()).build(creedoSession);
		creedoSession.uiRegister().registerUiComponent(optionManagementPage);
		adminFrameActions.add(new LoadPageAction(creedoSession.uiRegister().nextId(), optionManagementPage,
				adminFramePageContainer));

		RepositoryAdministrationPage<UserDetails> userManagementPage = new DefaultRepositoryAdministrationPageBuilder<UserDetails>()
				.setRepository(Users.detailsRepository())
				.setAdditionClasses(Arrays.asList((Class<DefaultUserDetails>) DefaultUserDetails.class))
				.setAdditionTypeAliases(Arrays.asList("User")).build(creedoSession);
		creedoSession.uiRegister().registerUiComponent(userManagementPage);
		adminFrameActions.add(new LoadPageAction(creedoSession.uiRegister().nextId(), userManagementPage,
				adminFramePageContainer));

		RepositoryAdministrationPage<WorkspaceBuilder> dataManagementPage = new DefaultRepositoryAdministrationPageBuilder<WorkspaceBuilder>()
				.setRepository(ApplicationRepositories.DATA_REPOSITORY)
				.setAdditionClasses(
						Arrays.asList(XarfBasedWorkspaceBuilder.class, FixedComponentWorkspaceBuilder.class))
				.setAdditionTypeAliases(Arrays.asList("xarf-based workspace", "legacy workspace")).build(creedoSession);
		creedoSession.uiRegister().registerUiComponent(dataManagementPage);
		adminFrameActions.add(new LoadPageAction(creedoSession.uiRegister().nextId(), dataManagementPage,
				adminFramePageContainer));

		RepositoryAdministrationPage<MiningSystemBuilder<?>> miningSystemManagementPage = new DefaultRepositoryAdministrationPageBuilder<MiningSystemBuilder<?>>()
				.setRepository(ApplicationRepositories.MINING_SYSTEM_REPOSITORY)
				.setAdditionClasses(ConfigurationProperties.get().MINING_SYSTEM_BUILDER_CLASSES)
				.setAdditionTypeAliases(ConfigurationProperties.get().MINING_SYSTEM_BUILDER_CLASSES.stream()
						.map(clazz -> clazz.getSimpleName()).collect(Collectors.toList()))
				.build(creedoSession);
		creedoSession.uiRegister().registerUiComponent(miningSystemManagementPage);
		adminFrameActions.add(new LoadPageAction(creedoSession.uiRegister().nextId(), miningSystemManagementPage,
				adminFramePageContainer));

		RepositoryAdministrationPage<DashboardLinkBuilder> demoManagementPage = new DefaultRepositoryAdministrationPageBuilder<DashboardLinkBuilder>()
				.setRepository(ApplicationRepositories.DEMO_REPOSITORY)
				.setAdditionClasses(Arrays.asList((Class<?>) DashboardLinkBuilder.class))
				.setAdditionTypeAliases(Arrays.asList("Demo link")).build(creedoSession);
		creedoSession.uiRegister().registerUiComponent(demoManagementPage);
		adminFrameActions.add(new LoadPageAction(creedoSession.uiRegister().nextId(), demoManagementPage,
				adminFramePageContainer));

		RepositoryAdministrationPage<StudyDesignBuilder> studyDesignManagementPage = new DefaultRepositoryAdministrationPageBuilder<StudyDesignBuilder>()
				.setRepository(ApplicationRepositories.STUDY_DESIGN_REPOSITORY)
				.setAdditionClasses(Arrays.asList((Class<?>) StudyDesignBuilder.class))
				.setAdditionTypeAliases(Arrays.asList("Study Design")).build(creedoSession);

		creedoSession.uiRegister().registerUiComponent(studyDesignManagementPage);
		adminFrameActions.add(new LoadPageAction(creedoSession.uiRegister().nextId(), studyDesignManagementPage,
				adminFramePageContainer));

		RepositoryAdministrationPage<TaskSpecBuilder> taskSpecManagementPage = new DefaultRepositoryAdministrationPageBuilder<TaskSpecBuilder>()
				.setRepository(ApplicationRepositories.STUDY_TASK_SPECIFICATION_REPOSITORY)
				.setAdditionClasses(Arrays.asList((Class<?>) TaskSpecification.class))
				.setAdditionTypeAliases(Arrays.asList("Task Specification")).build(creedoSession);

		creedoSession.uiRegister().registerUiComponent(taskSpecManagementPage);
		adminFrameActions.add(new LoadPageAction(creedoSession.uiRegister().nextId(), taskSpecManagementPage,
				adminFramePageContainer));

		RepositoryAdministrationPage<EvaluationScheme> evaluationSchemeManagementPage = new DefaultRepositoryAdministrationPageBuilder<EvaluationScheme>()
				.setRepository(ApplicationRepositories.STUDY_EVALUATION_SCHEME_REPOSITORY)
				.setAdditionClasses(Arrays.asList((Class<?>) EvaluationScheme.class))
				.setAdditionTypeAliases(Arrays.asList("Evaluation Scheme")).build(creedoSession);
		creedoSession.uiRegister().registerUiComponent(evaluationSchemeManagementPage);
		adminFrameActions.add(new LoadPageAction(creedoSession.uiRegister().nextId(),
				evaluationSchemeManagementPage, adminFramePageContainer));

		RepositoryAdministrationPage<SystemSpecBuilder> systemSpecManagementPage = new DefaultRepositoryAdministrationPageBuilder<SystemSpecBuilder>()
				.setRepository(ApplicationRepositories.STUDY_SYSTEM_SPECIFICATION_REPOSITORY)
				.setAdditionClasses(Arrays.asList((Class<?>) SystemSpecBuilder.class))
				.setAdditionTypeAliases(Arrays.asList("System Specification")).build(creedoSession);
		creedoSession.uiRegister().registerUiComponent(systemSpecManagementPage);
		adminFrameActions.add(new LoadPageAction(creedoSession.uiRegister().nextId(), systemSpecManagementPage,
				adminFramePageContainer));

		RepositoryAdministrationPage<StudyBuilder> studyManagementPage = new DefaultRepositoryAdministrationPageBuilder<StudyBuilder>()
				.setRepository(ApplicationRepositories.STUDY_REPOSITORY)
				.setAdditionClasses(Arrays.asList((Class<?>) StudyBuilder.class))
				.setAdditionTypeAliases(Arrays.asList("Study")).build(creedoSession);
		creedoSession.uiRegister().registerUiComponent(studyManagementPage);
		adminFrameActions.add(new LoadPageAction(creedoSession.uiRegister().nextId(), studyManagementPage,
				adminFramePageContainer));

		RepositoryAdministrationPage<RuntimeBuilder<Page, CreedoSession>> pageManagementPage = new DefaultRepositoryAdministrationPageBuilder<RuntimeBuilder<Page, CreedoSession>>()
				.setRepository(ApplicationRepositories.PAGE_REPOSITORY)
				.setAdditionClasses(Arrays.asList((Class<?>) ContentPageBuilder.class))
				.setAdditionTypeAliases(Arrays.asList("Content page")).build(creedoSession);
		creedoSession.uiRegister().registerUiComponent(pageManagementPage);
		adminFrameActions.add(new LoadPageAction(creedoSession.uiRegister().nextId(), pageManagementPage,
				adminFramePageContainer));

		adminFrameActions.add(new Action() {

			private int id = creedoSession.uiRegister().nextId();

			@Override
			public String getReferenceName() {
				return "Close";
			}

			@Override
			public ResponseEntity<String> activate(String... params) {
				return new ResponseEntity<String>(HttpStatus.OK);
			}

			@Override
			public ClientWindowEffect getEffect() {
				return Action.ClientWindowEffect.CLOSE;
			}

			@Override
			public int getId() {
				return id;
			}
		});

		return creedoSession.uiRegister().createDefaultFrame(adminFramePageContainer, adminFrameActions,
				new ArrayList<Action>());

	}
}
