package de.unibonn.creedo.ui;

import static de.unibonn.realkd.common.base.Identifier.id;
import static de.unibonn.realkd.common.parameter.Parameters.stringParameter;

import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.DynamicUrlController;
import de.unibonn.creedo.admin.options.Configurable;
import de.unibonn.creedo.admin.users.CreedoUser;
import de.unibonn.creedo.boot.XarfBasedWorkspaceBuilder;
import de.unibonn.creedo.repositories.IdentifierInRepositoryParameter;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.creedo.setup.ServerPaths;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.ActionProvider;
import de.unibonn.creedo.ui.core.DataProvider;
import de.unibonn.creedo.ui.core.DefaultAction;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.ui.core.UiRegister;
import de.unibonn.creedo.ui.indexpage.StoredDashboardLinkBuilder;
import de.unibonn.creedo.webapp.utils.ParameterToJsonConverter;
import de.unibonn.realkd.common.base.ValidationException;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.util.Predicates;

/**
 * Model class for all custom-dashboard-creation related logic.
 *
 * @author Bo Kang
 * @auhtor Björn Jacobs
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.6.0
 * 
 */
public class CustomDashboardCreationPage
		implements Page, UiComponent, DataProvider, ActionProvider, Configurable<CustomDashboardCreationPageOptions> {

	private class OtherOptionsContainer implements ParameterContainer {

		public final Parameter<String> imagePathParameter;

		public final Parameter<String> imageCreditsParameter;

		public final Parameter<String> analyticsDashboardParameter;

		public OtherOptionsContainer() {
			imagePathParameter = new IdentifierInRepositoryParameter<Path>(id("image"), "Image",
					"Image file for the link on index page.", ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
					Repositories.getIdIsFilenameWithExtensionPredicate(Repositories.IMAGE_FILE_EXTENSIONS));

			imageCreditsParameter = stringParameter(id("image_credits"), "Image credits",
					"Can be used to display credit information for the image.", "", Predicates.notNull(), "");

			analyticsDashboardParameter = new IdentifierInRepositoryParameter<>(id("analysis_system"),
					"Analysis system", "System to be loaded for analytics dashboard.",
					ApplicationRepositories.MINING_SYSTEM_REPOSITORY,
					entry -> CustomDashboardCreationPage.this.getOptions().systems().contains(entry.getId()));
		}

		@Override
		public List<Parameter<?>> getTopLevelParameters() {
			return ImmutableList.of(analyticsDashboardParameter, imagePathParameter, imageCreditsParameter);
		}

	}

	private static final Logger LOGGER = Logger.getLogger(CustomDashboardCreationPage.class.getName());

	private final XarfBasedWorkspaceBuilder xarfBasedWorkspaceBuilder;

	private final OtherOptionsContainer otherOptionsContainer;

	private final int id;

	private final int openActionId;

	private final CreedoUser user;

	private final UiRegister uiRegister;

	private final Action xarfParamsUpdateAction;

	private final Action otherOptionsUpdateAction;

	public CustomDashboardCreationPage(UiRegister uiRegister, CreedoUser user) {
		this.uiRegister = uiRegister;
		this.id = uiRegister.nextId();
		this.user = user;
		this.openActionId = uiRegister.nextId();
		this.xarfBasedWorkspaceBuilder = new XarfBasedWorkspaceBuilder();
		this.otherOptionsContainer = new OtherOptionsContainer();
		this.xarfParamsUpdateAction = DefaultAction.updateParameterAction(uiRegister.nextId(),
				xarfBasedWorkspaceBuilder);
		this.otherOptionsUpdateAction = DefaultAction.updateParameterAction(uiRegister.nextId(), otherOptionsContainer);
	}

	@Override
	public String getTitle() {
		return "Open Analytics Dashboard";
	}

	@Override
	public String getReferenceName() {
		return "Analyze";
	}

	@Override
	public String getViewImport() {
		return "dashboardCreationPage.jsp";
	}

	@Override
	public Model getModel() {
		Model model = new BindingAwareModelMap();
		model.addAttribute("componentId", id);
		model.addAttribute("xarfparamsUpdateActionId", xarfParamsUpdateAction.getId());
		model.addAttribute("otheroptionsUpdateActionId", otherOptionsUpdateAction.getId());
		model.addAttribute("openActionId", openActionId);
		return model;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getView() {
		return getViewImport();
	}

	@Override
	public ImmutableSet<String> getOwnScriptFilenames() {
		return ImmutableSet.of("creedo-parameters.js?v=2", "creedo-customdashboard.js");
	}

	@Override
	public ImmutableSet<String> getDataItemIds() {
		return ImmutableSet.of("xarfparams", "otheroptions");
	}

	@Override
	public List<? extends Object> getDataItem(String dataItemId) {
		if (dataItemId.equals("xarfparams")) {
			return ParameterToJsonConverter.convertRecursively(xarfBasedWorkspaceBuilder.getTopLevelParameters());
		} else if (dataItemId.equals("otheroptions")) {
			return ParameterToJsonConverter.convertRecursively(otherOptionsContainer.getTopLevelParameters());
		}
		throw new IllegalArgumentException("unknown parameter");
	}

	@Override
	public Collection<Integer> getActionIds() {
		return ImmutableList.of(openActionId);
	}

	@Override
	public ResponseEntity<String> performAction(int id, String... params) {
		if (id == openActionId) {
			try {
				xarfBasedWorkspaceBuilder.validate();
				otherOptionsContainer.validate();
				Optional<Path> path = ServerPaths.newWorkspaceDirectory(user);
				if (!path.isPresent()) {
					LOGGER.warning("Could not acquire path for storing dashboard data.");
					// here need to clone, respectively just open!!
					// dataSupplier = xarfBasedWorkspaceBuilder;
				} else {
					xarfBasedWorkspaceBuilder.backupPath(path.get());
					xarfBasedWorkspaceBuilder.get();
					StoredDashboardLinkBuilder storedDbLinkBuilder = new StoredDashboardLinkBuilder()
							.title(path.get().getFileName().toString()).description("custom dashboard")
							.imageCredits(otherOptionsContainer.imageCreditsParameter.current())
							.imagePath(otherOptionsContainer.imagePathParameter.current())
							.miningSystem(otherOptionsContainer.analyticsDashboardParameter.current())
							.workspaceDirectory(path.get().getFileName().toString()).user(user);
					ApplicationRepositories.STORED_DASHBOARD_REPOSITORY.add(path.get().getFileName().toString(),
							storedDbLinkBuilder);

					if (uiRegister.analyticsDashboardOpen()) {
						return new ResponseEntity<String>(
								"Another analytics dashboard already open. Please close first.", HttpStatus.FORBIDDEN);
					}
					uiRegister.createMiningDashboard(storedDbLinkBuilder.get().getAnalyticsDashboardBuilder());
					return new ResponseEntity<String>(DynamicUrlController.frameUrl(uiRegister.getDashboard().getId()),
							HttpStatus.OK);
				}

			} catch (ValidationException e) {
				throw new IllegalStateException(e);
			}
		} else if (id == xarfParamsUpdateAction.getId()) {
			return xarfParamsUpdateAction.activate(params);
		} else if (id == otherOptionsUpdateAction.getId()) {
			return otherOptionsUpdateAction.activate(params);
		}

		throw new IllegalArgumentException("no such action");
	}

	@Override
	public CustomDashboardCreationPageOptions getDefaultOptions() {
		return new CustomDashboardCreationPageOptions();
	}

}
