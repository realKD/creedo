package de.unibonn.creedo.ui.standard;

import static de.unibonn.realkd.common.parameter.Parameters.stringParameter;
import static de.unibonn.realkd.common.base.Identifier.id;

import java.util.List;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.Creedo;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;

public class FooterOptions implements ParameterContainer {

	private Parameter<String> copyrightStatement = stringParameter(id("Copyright_notice"), "Copyright notice",
			"Copyright line that is shown along with the footer links and the version infos.", Creedo.copyright(),
			x -> true, "");

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return ImmutableList.of(copyrightStatement);
	}

	public String getCopyrightNotice() {
		return copyrightStatement.current();
	}

}
