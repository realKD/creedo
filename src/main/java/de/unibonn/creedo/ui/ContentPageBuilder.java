package de.unibonn.creedo.ui;

import static de.unibonn.realkd.common.parameter.Parameters.stringParameter;
import static de.unibonn.realkd.common.base.Identifier.id;

import java.util.List;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.IdentifierInRepositoryParameter;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.creedo.setup.ServerPaths;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.realkd.common.RuntimeBuilder;
import de.unibonn.realkd.common.parameter.DefaultParameterContainer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.util.Predicates;

/**
 *
 * @author bjacobs
 * 
 */
public class ContentPageBuilder implements PageBuilder, ParameterContainer,
		RuntimeBuilder<Page, CreedoSession> {

	private final DefaultParameterContainer parameterContainer = new DefaultParameterContainer();

	private final Parameter<String> title;
	private final Parameter<String> referenceName;
	private final Parameter<String> contentPageFileName;

	public ContentPageBuilder() {
		title = stringParameter(id("Title"), "Title",
				"Title to be displayed above page content.", "",
				Predicates.satisfied(), "");

		referenceName = stringParameter(id("Reference_name"), "Reference name",
				"Short name for links that reference page.", "",
				Predicates.satisfied(), "");

		contentPageFileName = new IdentifierInRepositoryParameter<>(
				id("Content_file_name"),
				"Content file name",
				"The main content diplayed by the page",
				ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
				Repositories
						.getIdIsFilenameWithExtensionPredicate(Repositories.HTML_FILE_EXTENSIONS));
		// new
		// IdIsFilenameWithExtension<Path>(IdIsFilenameWithExtension.HTML_FILE_EXTENSIONS));

		parameterContainer.addParameter(title);
		parameterContainer.addParameter(referenceName);
		parameterContainer.addParameter(contentPageFileName);
	}

	@Override
	public Page build(CreedoSession session) {
		return new HtmlPage(title.current(),
				referenceName.current(),
				ServerPaths.contentFileContentAsString(contentPageFileName
						.current()));
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return parameterContainer.getTopLevelParameters();
	}

}
