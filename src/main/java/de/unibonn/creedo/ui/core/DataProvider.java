package de.unibonn.creedo.ui.core;

import java.util.List;

import com.google.common.collect.ImmutableSet;

import de.unibonn.creedo.DynamicUrlController;

/**
 * <p>
 * Interface for ui components that provide data for dynamic or delayed loading
 * from client. Client-side JavaScript code can request data via
 * CREEDO.core.requestData.
 * </p>
 * 
 * <p>
 * Request handled by {@link DynamicUrlController#getData(componentId, dataItemId)},
 * which resolves request by first locating DataProvider object via the ui
 * register of the creedo session and then calling {@link #getDataItem(String)}
 * if there is data provider with component id.
 * </p>
 * 
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @version 0.1.2.1
 *
 */
public interface DataProvider extends UiComponent {

	/**
	 * Provides set of all data item identifiers supported by this data
	 * provider.
	 */
	public ImmutableSet<String> getDataItemIds();

	public default boolean hasDataItem(String dataItem) {
		return getDataItemIds().contains(dataItem);
	}

	/**
	 * Returns the data package specified by id. Format must be convertible into
	 * json by Spring, i.e., list of bean objects.
	 * 
	 * @param dataItemId
	 *            identifier of requested data item
	 * @return data as list of bean objects that can be transformed into json
	 */
	public List<? extends Object> getDataItem(String dataItemId);

}
