package de.unibonn.creedo.ui.core;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.function.Function;
import java.util.logging.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import de.unibonn.creedo.common.parameters.SecureParameter;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;

/**
 * <p>
 * Default implementation of {@link Action}. Interfaces with JSP. Therefore uses
 * classic Java conventions.
 * </p>
 * 
 * @author Mario Boley
 * 
 * @since 0.4.0
 * 
 * @version 0.4.0
 *
 */
public class DefaultAction implements Action {
	
	private static final Logger LOGGER=Logger.getLogger(DefaultAction.class.getName());

	public static DefaultAction action(int id, String referenceName, Function<String[], ResponseEntity<String>> action,
			ClientWindowEffect effect) {
		return new DefaultAction(id, referenceName, action, effect, null, false);
	}

	public static DefaultAction action(int id, String referenceName, Function<String[], ResponseEntity<String>> action,
			ClientWindowEffect effect, boolean confirmed) {
		return new DefaultAction(id, referenceName, action, effect, null, confirmed);
	}

	public static DefaultAction action(int id, String referenceName, Function<String[], ResponseEntity<String>> action,
			ClientWindowEffect effect, boolean confirmed, String inputParameterName) {
		return new DefaultAction(id, referenceName, action, effect, inputParameterName, confirmed);
	}

	public static DefaultAction updateParameterAction(int id, ParameterContainer container) {
		return action(id, "update " + container.toString(), params -> {
			checkArgument(params.length == 2);
			checkArgument(params[0] != null);

			String parameterName = params[0];
			String value = params[1];

			LOGGER.fine("Parameter change action: " + parameterName + "->" + value);
			Parameter<?> parameter = container.findParameterByName(parameterName);

			// Polymorhpic refactoring here: eliminate SecureParameter and add
			// setByClearValue to Parameter
			if (parameter instanceof SecureParameter) {
				((SecureParameter<?>) parameter).setByClearValue(value);
			} else {
				parameter.setByString(value);
			}
			return new ResponseEntity<>(HttpStatus.OK);
		}, ClientWindowEffect.REFRESH);
	}

	private final int id;

	private final String referenceName;

	private final Function<String[], ResponseEntity<String>> action;

	private final ClientWindowEffect effect;

	private final String inputParameterName;

	private final boolean confirmed;

	private DefaultAction(int id, String referenceName, Function<String[], ResponseEntity<String>> action,
			ClientWindowEffect effect, String inputParameterName, boolean confirmed) {
		this.id = id;
		this.referenceName = referenceName;
		this.action = action;
		this.effect = effect;
		this.inputParameterName = inputParameterName;
		this.confirmed = confirmed;
	}

	@Override
	public String getReferenceName() {
		return referenceName;
	}

	@Override
	public ResponseEntity<String> activate(String... params) {
		return action.apply(params);
	}

	@Override
	public ClientWindowEffect getEffect() {
		return effect;
	}

	@Override
	public int getId() {
		return id;
	}

	public boolean getHasInputParameter() {
		return inputParameterName != null;
	}

	public String getInputParameterName() {
		return inputParameterName;
	}

	public boolean getConfirmed() {
		return confirmed;
	}

}
