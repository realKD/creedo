package de.unibonn.creedo.ui.core;

import org.springframework.web.servlet.ModelAndView;

public interface Frame extends UiComponent {

	public default ModelAndView getModelAndView() {
		return new ModelAndView(getView(), getModel().asMap());
	}

}
