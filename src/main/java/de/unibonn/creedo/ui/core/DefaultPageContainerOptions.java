package de.unibonn.creedo.ui.core;

import static de.unibonn.realkd.common.parameter.Parameters.stringParameter;
import static de.unibonn.realkd.common.base.Identifier.id;

import java.util.List;
import java.util.Optional;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.IdentifierInRepositoryParameter;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.Parameters;

public class DefaultPageContainerOptions implements ParameterContainer {

	private final Parameter<Optional<String>> primaryLogoFilename;
	private final Parameter<String> primaryLogoLinkUrl;
	private final Parameter<Optional<String>> secondaryLogoFilename;
	private final Parameter<String> secondaryLogoLinkUrl;
	private final Parameter<Integer> pageWidth;

	public DefaultPageContainerOptions() {
		this.primaryLogoFilename = IdentifierInRepositoryParameter.optionalRepositoryIdParameter(id("Primary_logo"),
				"Primary logo", "Optional logo image file to be displayed along with page title",
				ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
				Repositories.getIdIsFilenameWithExtensionPredicate(Repositories.IMAGE_FILE_EXTENSIONS));
		this.primaryLogoLinkUrl = stringParameter(id("Primary_logo_link"), "Primary logo link",
				"Hyperlink for primary logo (ignored if no primary logo selected)", "", s -> true, "");
		this.secondaryLogoFilename = IdentifierInRepositoryParameter.optionalRepositoryIdParameter(id("Secondary_logo"),
				"Secondary logo", "Optional additional logo image file to be displayed along with page title",
				ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
				Repositories.getIdIsFilenameWithExtensionPredicate(Repositories.IMAGE_FILE_EXTENSIONS));
		this.secondaryLogoLinkUrl = stringParameter(id("Secondary_logo_link"), "Secondary logo link",
				"Hyperlink for secondary logo (ignored if no secondary logo selected)", "", s -> true, "");
		this.pageWidth = Parameters.rangeEnumerableParameter(id("Page_width"), "Page width", "Bootstrap page width.",
				Integer.class, () -> ImmutableList.of(6, 8, 10, 12));
		this.pageWidth.set(8);
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return ImmutableList.of(pageWidth, primaryLogoFilename, primaryLogoLinkUrl, secondaryLogoFilename,
				secondaryLogoLinkUrl);
	}

	public Optional<String> getPrimaryLogoFilename() {
		return primaryLogoFilename.current();
	}

	public Optional<String> getSecondaryLogoFilename() {
		return secondaryLogoFilename.current();
	}

	public String getPrimaryLogoLink() {
		return primaryLogoLinkUrl.current();
	}

	public String getSecondaryLogoLink() {
		return secondaryLogoLinkUrl.current();
	}

	public Integer pageWidth() {
		return pageWidth.current();
	}

}
