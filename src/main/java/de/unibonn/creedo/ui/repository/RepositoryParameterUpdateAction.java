package de.unibonn.creedo.ui.repository;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.logging.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import de.unibonn.creedo.common.parameters.SecureParameter;
import de.unibonn.creedo.repositories.Repository;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;

public class RepositoryParameterUpdateAction<T> implements Action {

	private final static Logger LOGGER = Logger.getLogger(RepositoryParameterUpdateAction.class.getName());

	private final int id;
	private final String pageBuilderId;
	private final Repository<String, T> repository;

	public RepositoryParameterUpdateAction(int id, Repository<String, T> repository, String containerId) {
		this.id = id;
		this.pageBuilderId = containerId;
		this.repository = repository;
	}

	@Override
	public String getReferenceName() {
		return "Update";
	}

	@Override
	public ResponseEntity<String> activate(String... params) {
		checkArgument(params.length == 2);
		checkArgument(params[0] != null);

		String parameterName = params[0];
		String value = params[1];

		LOGGER.fine("Parameter change action: " + parameterName + "->" + value);

		RepositoryEntry<String, T> entry = repository.getEntry(pageBuilderId);
		ParameterContainer container = (ParameterContainer) entry.getContent();
		Parameter<?> parameter = container.findParameterByName(parameterName);

		// Polymorhpic refactoring here: eliminate SecureParameter and add
		// setByClearValue to Parameter
		if (parameter instanceof SecureParameter) {
			((SecureParameter<?>) parameter).setByClearValue(value);
		} else {
			parameter.setByString(value);
		}

		repository.update(entry);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	public ClientWindowEffect getEffect() {
		return ClientWindowEffect.REFRESH;
	}

	@Override
	public int getId() {
		return id;
	}

}