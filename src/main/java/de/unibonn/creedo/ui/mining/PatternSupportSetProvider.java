package de.unibonn.creedo.ui.mining;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.ui.Model;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import de.unibonn.creedo.ui.core.DataProvider;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.realkd.discovery.Discovery;
import de.unibonn.realkd.patterns.LocalPatternDescriptor;
import de.unibonn.realkd.patterns.PatternDescriptor;

public class PatternSupportSetProvider implements DataProvider {

	private final int id;

	private final Map<Integer, Discovery> idToWebPattern;

	public PatternSupportSetProvider(int id, Map<Integer, Discovery> patternMap) {
		this.id = id;
		this.idToWebPattern = patternMap;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getView() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Model getModel() {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<UiComponent> getComponents() {
		return ImmutableList.of();
	}

	@Override
	public ImmutableSet<String> getDataItemIds() {
		return ImmutableSet
				.copyOf(idToWebPattern.keySet().stream().map(x -> String.valueOf(x)).collect(Collectors.toList()));
	}

	@Override
	public List<Object> getDataItem(String dataItemId) {
		PatternDescriptor descriptor = idToWebPattern.get(Integer.parseInt(dataItemId)).content().descriptor();
		return ImmutableList.copyOf(((LocalPatternDescriptor) descriptor).supportSet());
	}

}
