package de.unibonn.creedo.ui.mining;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.ActionProvider;
import de.unibonn.creedo.ui.core.DataProvider;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.ui.core.UiRegister.IdGenerator;
import de.unibonn.creedo.webapp.utils.ParameterToJsonConverter;
import de.unibonn.creedo.webapp.viewmodels.DataTableViewModel;
import de.unibonn.creedo.webapp.viewmodels.MetaDataTableModel;
import de.unibonn.creedo.webapp.viewmodels.PointCloudViewModel;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.data.propositions.TableBasedPropositionalContext;
import de.unibonn.realkd.data.table.DataTable;

/**
 * UI component that aggregates several views on a data workspace.
 * 
 * @author Mario Boley
 * @author Elvin Efendijev
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2.1
 *
 */
public class DataViewContainer implements UiComponent, DataProvider, ActionProvider {

	private static final Logger LOGGER = Logger.getLogger(DataViewContainer.class.getName());

	private final int id;

	private DataTableViewModel dataTableViewModel;

	private MetaDataTableModel metaDataModel;

	private PointCloudViewModel pointCloudViewModel;

	private final Workspace workspace;

	private final HttpSession httpSession;

	private final DataWorkSpaceTooltip tooltip;

	private final Action pointCloudParameterUpdateAction;
	
	private final boolean pcaAsScatterDefault;

	public DataViewContainer(IdGenerator idGenerator, HttpSession httpSession, Workspace dataWorkspace, boolean pcaAsScatterDefault) {
		this.id = idGenerator.getNextId();
		this.httpSession = httpSession;
		this.workspace = dataWorkspace;
		this.tooltip = new DataWorkSpaceTooltip(dataWorkspace);
		this.pcaAsScatterDefault=pcaAsScatterDefault;
		this.pointCloudParameterUpdateAction = new Action() {

			private final int id = idGenerator.getNextId();

			@Override
			public String getReferenceName() {
				return "Set parameter";
			}

			@Override
			public ResponseEntity<String> activate(String... params) {
				checkArgument(params.length == 2);
				checkArgument(params[0] != null);

				String parameterName = params[0];
				String value = params[1];

				pointCloudViewModel().findParameterByName(parameterName).setByString(value);

				return new ResponseEntity<>(HttpStatus.OK);
			}

			@Override
			public ClientWindowEffect getEffect() {
				return ClientWindowEffect.NONE;
			}

			@Override
			public int getId() {
				return id;
			}
		};
	}

	public DataTable getDataTable() {
		return workspace.datatables().get(0);
	}

	public PropositionalContext getPropositionalLogic() {
		return workspace.propositionalContexts().get(0);
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getView() {
		return "../static/dataViewContainer.jsp";
	}

	@Override
	public Model getModel() {
		BindingAwareModelMap model = new BindingAwareModelMap();
		model.addAttribute("datasetName", getDataTable().name());
		model.addAttribute("dataViewContainerId", this.getId());
		model.addAttribute("dataTableViewModel", dataTableViewModel());
		model.addAttribute("metaDataModel", metaDataModel());
		model.addAttribute("updatePointCloudActionId", pointCloudParameterUpdateAction.getId());
		return model;
	}

	@Override
	public List<UiComponent> getComponents() {
		return Arrays.asList();
	}

	private PointCloudViewModel pointCloudViewModel() {
		if (pointCloudViewModel == null) {
			pointCloudViewModel = new PointCloudViewModel(workspace,getDataTable(),pcaAsScatterDefault);
		}
		return pointCloudViewModel;
	}

	private DataTableViewModel dataTableViewModel() {
		if (dataTableViewModel == null) {
			dataTableViewModel = new DataTableViewModel(getDataTable(), httpSession);
		}
		return dataTableViewModel;
	}

	private MetaDataTableModel metaDataModel() {
		if (metaDataModel == null) {
			if (getPropositionalLogic() instanceof TableBasedPropositionalContext) {
				metaDataModel = new MetaDataTableModel((TableBasedPropositionalContext) getPropositionalLogic());
			}
		}
		return metaDataModel;
	}

	@Override
	public ImmutableSet<String> getOwnScriptFilenames() {
		return ImmutableSet.of("creedo-dataviewcontainer.js?v=66");
	}

	@Override
	public ImmutableSet<String> getDataItemIds() {
		return ImmutableSet.of("tableData", "columnTooltips", "pointData", "pointParameters", "pointAxesTitles",
				"description");
	}

	private static class DataWorkSpaceTooltip {

		String tooltip;

		public DataWorkSpaceTooltip(Workspace dataWorkspace) {
			StringBuilder tooltipBuilder = new StringBuilder();
			tooltipBuilder.append("<p>" + dataWorkspace.datatables().get(0).description() + "</p>");
			tooltipBuilder.append("<hr class='no-margin' />");
			tooltipBuilder.append("<br />");
			tooltipBuilder.append("<p>");
			tooltipBuilder.append(dataWorkspace.datatables().get(0).population().size() + " rows<br />\n");
			tooltipBuilder
					.append(dataWorkspace.datatables().get(0).numberOfAttributes() + " attributes<br />\n");
			tooltipBuilder.append(dataWorkspace.propositionalContexts().get(0).propositions().size()
					+ " propositions<br />\n");
			tooltipBuilder.append("</p>");
			tooltip = tooltipBuilder.toString();
		}

		public String toString() {
			return tooltip;
		}

	}

	/**
	 * <p>
	 * <b>Item 'tableData':</b> returns the data in the DataTable in its
	 * original order. Do not change the order here since it will break the
	 * data-consistency with clients. <br>
	 * FUTURE: implement server side processing for DataTables:
	 * http://datatables.net/manual/server-side (elvin)
	 * </p>
	 * 
	 */
	@Override
	public List<? extends Object> getDataItem(String dataItem) {
		LOGGER.fine("Checking for data item '" + dataItem + "'");
		if (dataItem.equals("tableData")) {
			return ImmutableList.copyOf(dataTableViewModel().getDataTableContent());
		} else if (dataItem.equals("pointParameters")) {
			return ParameterToJsonConverter.convertRecursively(pointCloudViewModel().getTopLevelParameters());
		} else if (dataItem.equals("pointData")) {
			return ImmutableList.copyOf(pointCloudViewModel().points());
		} else if (dataItem.equals("pointAxesTitles")) {
			return pointCloudViewModel().getAxisTitles();
		} else if (dataItem.equals("columnTooltips")) {
			return ImmutableList.copyOf(dataTableViewModel().getColumnToolTipHtml());
		} else if (dataItem.equals("description")) {
			return ImmutableList.of(tooltip.toString());
		} else {
			throw new IllegalArgumentException("No such data item.");
		}
	}

	@Override
	public Collection<Integer> getActionIds() {
		return ImmutableSet.of(pointCloudParameterUpdateAction.getId());
	}

	@Override
	public ResponseEntity<String> performAction(int id, String... params) {
		if (pointCloudParameterUpdateAction.getId() == id) {
			return pointCloudParameterUpdateAction.activate(params);
		}
		return new ResponseEntity<>("unknown action", HttpStatus.BAD_REQUEST);
	}

}
