package de.unibonn.creedo.ui.mining;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.google.common.collect.ImmutableSet;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.ActionProvider;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.webapp.dashboard.mining.MiningSystem;
import de.unibonn.creedo.webapp.patternviews.CandidatePatternGenerator;
import de.unibonn.creedo.webapp.patternviews.PatternHTMLGenerator;
import de.unibonn.creedo.webapp.patternviews.ResultPatternGenerator;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.PropositionalContext;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.discovery.Discovery;
import de.unibonn.realkd.patterns.Pattern;

/**
 * Aggregates UI components for performing data analysis.
 * 
 * @author Mario Boley
 * 
 * @since 0.2.0
 * 
 * @version 0.4.0
 *
 */
public class AnalyticsDashboard implements UiComponent, ActionProvider {

	// private static final String RESULT_PATTERN_ENTITY_ID = "$results";

	private static final Logger LOGGER = Logger.getLogger(AnalyticsDashboard.class.getName());

	private class RunMiningAlgorithmAction implements Action {

		private final int id;

		public RunMiningAlgorithmAction(int id) {
			this.id = id;
		}

		@Override
		public String getReferenceName() {
			throw new UnsupportedOperationException();
		}

		@Override
		public ResponseEntity<String> activate(String... params) {
			try {
				return new ResponseEntity<String>(mineClicked(), HttpStatus.OK);
			} catch (RuntimeException e) {
				return new ResponseEntity<>(e.getMessage() + "\n" + Arrays.toString(e.getStackTrace()),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

		@Override
		public ClientWindowEffect getEffect() {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getId() {
			return id;
		}

	}

	private class AnnotatePatternAction implements Action {

		private final int id;

		public AnnotatePatternAction(int id) {
			this.id = id;
		}

		@Override
		public String getReferenceName() {
			throw new UnsupportedOperationException();
		}

		@Override
		public ResponseEntity<String> activate(String... params) {
			checkArgument(params.length == 2, "Params must exactly contain one pattern id and one annotation string");

			int id = Integer.parseInt(params[0]);
			String annotation = params[1];
			// To prevent insertion of html tags
			annotation = annotation.replaceAll("<", " smaller ").replaceAll(">", " larger ");

			Discovery pattern = miningSystem.getDiscoveryProcess().state().discovery(id);
			if (pattern == null) {
				return new ResponseEntity<String>("No such pattern", HttpStatus.BAD_REQUEST);
			}
			pattern.description(annotation);

			LOGGER.log(Level.INFO, "Received pattern annotation '" + annotation + "' for pattern id '" + id + "'");

			return new ResponseEntity<>(HttpStatus.OK);
		}

		@Override
		public ClientWindowEffect getEffect() {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getId() {
			return id;
		}

	}

	private class SavePatternAction implements Action {

		private final int id;

		public SavePatternAction(int id) {
			this.id = id;
		}

		@Override
		public String getReferenceName() {
			throw new UnsupportedOperationException();
		}

		@Override
		public ResponseEntity<String> activate(String... params) {
			checkArgument(params.length == 1, "Params must exactly contain one pattern id");
			LOGGER.log(Level.INFO, "Saving pattern " + params[0]);
			int id = Integer.parseInt(params[0]);
			saveToResults(id);
			// String html = getPattern(id).getHtml(httpSession);
			String html = resultPatternHtmlGenerator.getHTML(httpSession, getPattern(id));
			return new ResponseEntity<String>(html, HttpStatus.OK);
		}

		@Override
		public ClientWindowEffect getEffect() {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getId() {
			return id;
		}

	}

	private class DeletePatternAction implements Action {

		private final int id;

		public DeletePatternAction(int id) {
			this.id = id;
		}

		@Override
		public String getReferenceName() {
			throw new UnsupportedOperationException();
		}

		@Override
		public ResponseEntity<String> activate(String... params) {
			checkArgument(params.length == 1, "params must contain exactly 1 pattern id");
			int deletedItemID = Integer.parseInt(params[0]);
			Discovery deletedWebPattern = getPattern(deletedItemID);
			boolean inResults = getMiningSystem().getDiscoveryProcess().state()
					.isInResults(deletedWebPattern.content());
			if (inResults) {
				discardFromResults(deletedItemID);
			} else {
				discardFromCandidates(deletedItemID);
			}

			LOGGER.log(Level.INFO,
					"Deleted pattern" + deletedItemID + "' from " + (inResults ? "results" : "candidates"));

			return new ResponseEntity<String>(HttpStatus.OK);
		}

		@Override
		public ClientWindowEffect getEffect() {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getId() {
			return id;
		}

	}

	private final HttpSession httpSession;

	private final long instantiationTimeTag;

	private final HashMap<Pattern<?>, Long> patternToMillisecondsUntilSaved;

	private final Action savePatternAction;
	private final Action runAlgorithmAction;
	private final Action deletePatternAction;
	private final Action annotatePatternAction;
	private final Action detailedPatternViewAction;

	private final Map<Integer, Action> idToActionMap;

	private final MiningSystem miningSystem;
	private final DataViewContainer dataViewContainer;
	private final PatternSupportSetProvider supportSetProvider;
	private final Workspace workspace;

	private final PatternHTMLGenerator candidatePatternHtmlGenerator;
	private final PatternHTMLGenerator resultPatternHtmlGenerator;

	private final int id;

	public AnalyticsDashboard(int id, HttpSession httpSession, Workspace workspace, MiningSystem miningSystem) {
		this.httpSession = httpSession;
		this.id = id;
		this.instantiationTimeTag = System.currentTimeMillis();
		this.patternToMillisecondsUntilSaved = new HashMap<>();

		this.workspace = workspace;
		this.miningSystem = miningSystem;
		this.dataViewContainer = new DataViewContainer(Creedo.session(httpSession).uiRegister().idGenerator(),
				httpSession, workspace, miningSystem.pcaAsScatterDefault());
		this.supportSetProvider = new PatternSupportSetProvider(Creedo.session(httpSession).uiRegister().nextId(),
				miningSystem.getDiscoveryProcess().state().discoveryMap());

		this.runAlgorithmAction = new RunMiningAlgorithmAction(Creedo.session(httpSession).uiRegister().nextId());
		this.savePatternAction = new SavePatternAction(Creedo.session(httpSession).uiRegister().nextId());
		this.deletePatternAction = new DeletePatternAction(Creedo.session(httpSession).uiRegister().nextId());
		this.annotatePatternAction = new AnnotatePatternAction(Creedo.session(httpSession).uiRegister().nextId());
		this.detailedPatternViewAction = new OpenDetailedPatternViewAction(
				Creedo.session(httpSession).uiRegister().nextId(),
				miningSystem.getDiscoveryProcess().state().discoveryMap(), httpSession);

		this.idToActionMap = new HashMap<>();
		this.idToActionMap.put(runAlgorithmAction.getId(), runAlgorithmAction);
		this.idToActionMap.put(savePatternAction.getId(), savePatternAction);
		this.idToActionMap.put(deletePatternAction.getId(), deletePatternAction);
		this.idToActionMap.put(annotatePatternAction.getId(), annotatePatternAction);
		this.idToActionMap.put(detailedPatternViewAction.getId(), detailedPatternViewAction);

		this.candidatePatternHtmlGenerator = new CandidatePatternGenerator(getId(), detailedPatternViewAction.getId(),
				supportSetProvider.getId());
		this.resultPatternHtmlGenerator = new ResultPatternGenerator(getId(), detailedPatternViewAction.getId(),
				supportSetProvider.getId());

//		Optional<NamedPatternCollection> initialResults = workspace.get(RESULT_PATTERN_ENTITY_ID,
//				NamedPatternCollection.class);
//		if (initialResults.isPresent()) {
//			miningSystem.getDiscoveryProcess().results(initialResults.get().patterns());
//			LOGGER.info("Deserialized " + initialResults.get().patterns().size() + " result pattern(s).");
//		}
	}

	@Override
	public String getView() {
		return "analyticsDashboard.jsp";
	}

	@Override
	public Model getModel() {
		Model model = new BindingAwareModelMap();
		String datasetName = getDataTable().name();

		model.addAllAttributes(dataViewContainer.getModel().asMap());
		model.addAllAttributes(miningSystem.getModel().asMap());

		model.addAttribute("title", datasetName);
		model.addAttribute("analyticsDashboardId", getId());

		model.addAttribute("runAlgorithmActionId", runAlgorithmAction.getId());
		model.addAttribute("savePatternActionId", savePatternAction.getId());
		model.addAttribute("deletePatternActionId", deletePatternAction.getId());
		model.addAttribute("annotatePatternActionId", annotatePatternAction.getId());

		model.addAttribute("resultPatternHtml", getResultPatternsAsHTML());
		model.addAttribute("candidatePatternHtml", getCandidatePatternsAsHTML());

		// model.addAttribute("openDetailedPatternViewActionId",
		// deletePatternAction.getId());

		return model;
	}

	public MiningSystem getMiningSystem() {
		return miningSystem;
	}

	public DataTable getDataTable() {
		return dataViewContainer.getDataTable();
	}

	public PropositionalContext getPropositionalLogic() {
		return dataViewContainer.getPropositionalLogic();
	}

	@Override
	public int getId() {
		return this.id;
	}

	@Override
	public List<UiComponent> getComponents() {
		return Arrays.asList((UiComponent) dataViewContainer, miningSystem, supportSetProvider);
	}

	public void saveToResults(int patternId) {
		long timeUntilSaved = System.currentTimeMillis() - instantiationTimeTag;
		Discovery discovery = miningSystem.getDiscoveryProcess().state().discovery(patternId);
		// workspace.add(discovery);
		patternToMillisecondsUntilSaved.put(discovery.content(), timeUntilSaved);
		getMiningSystem().getDiscoveryProcess().saveCandidate(discovery.content());
	}

	public void discardFromResults(int patternId) {
		//most certainly a bug
		patternToMillisecondsUntilSaved.remove(patternId);
		Discovery webPattern = getMiningSystem().getDiscoveryProcess().state().discovery(patternId);
		getMiningSystem().getDiscoveryProcess().deleteFromResults(webPattern.content());
	}

	public void discardFromCandidates(int patternId) {
		Discovery webPattern = getMiningSystem().getDiscoveryProcess().state().discovery(patternId);
		getMiningSystem().getDiscoveryProcess().deleteFromCandidates(webPattern.content());
	}

	private String getResultPatternsAsHTML() {
		StringBuilder html = new StringBuilder();
		for (Discovery webPattern : miningSystem.getDiscoveryProcess().state().results()) {
			html.append(resultPatternHtmlGenerator.getHTML(httpSession, webPattern));
		}
		return html.toString();
	}

	private String getCandidatePatternsAsHTML() {
		StringBuilder html = new StringBuilder();
		List<Discovery> webPatterns = miningSystem.getDiscoveryProcess().state().candidates();
		for (Discovery webPattern : webPatterns) {
			html.append(candidatePatternHtmlGenerator.getHTML(httpSession, webPattern));
		}

		return html.toString();
	}

	public String mineClicked() {
		getMiningSystem().getDiscoveryProcess().endRound();
		List<Pattern<?>> minedPatterns = getMiningSystem().mineClicked();
		getMiningSystem().getDiscoveryProcess().nextRound(minedPatterns);
		return this.getCandidatePatternsAsHTML();
	}

	private Discovery getPattern(int id) {
		Discovery result = miningSystem.getDiscoveryProcess().state().discovery(id);
		if (result == null) {
			throw new IllegalArgumentException("MiningDashboard does not contain pattern with matching id");
		}
		return result;
	}

	public long getMillisecondsUntilSaved(Pattern<?> pattern) {
		Long result = patternToMillisecondsUntilSaved.get(pattern);
		if (result == null) {
			throw new IllegalArgumentException("MiningDashboard does not contain requested pattern");
		}

		return result;
	}

	@Override
	public Collection<Integer> getActionIds() {
		return idToActionMap.keySet();
	}

	@Override
	public ResponseEntity<String> performAction(int id, String... params) {
		checkArgument(isActionAvailable(id), "action unavailable");
		return idToActionMap.get(id).activate(params);
	}

	@Override
	public ImmutableSet<String> getOwnScriptFilenames() {
		return ImmutableSet.of("creedo-patterns.js?v=0.6.0.1", "creedo-analyticsdashboard.js");
	}

	public List<Pattern<?>> getResultPatterns() {
		return getMiningSystem().getDiscoveryProcess().state().resultPatterns();
	}

	public List<Integer> getSecondsUntilSaved() {
		List<Integer> secondsUntilSaved = new ArrayList<>();

		for (Pattern<?> pattern : getResultPatterns()) {
			long millisecondsUntilSaved = getMillisecondsUntilSaved(pattern);
			secondsUntilSaved.add((int) millisecondsUntilSaved / 1000);
		}
		return secondsUntilSaved;
	}

	public void tearDown() {
		LOGGER.info("Storing discovery process state in workspace");
		// NamedPatternCollection resultCollection = new
		// NamedPatternCollection(RESULT_PATTERN_ENTITY_ID, "Results",
		// "Result patterns produced by user.",
		// miningSystem.getDiscoveryProcess().state().resultPatterns());
		workspace.overwrite(miningSystem.getDiscoveryProcess().state());
	}

}
