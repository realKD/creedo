package de.unibonn.creedo.ui.indexpage;

import static de.unibonn.realkd.common.parameter.Parameters.booleanParameter;
import static de.unibonn.realkd.common.base.Identifier.id;

import java.util.List;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;

public class DemoDashboardLinkOptions implements ParameterContainer {

	private final Parameter<Boolean> buildCopyOnOpen;

	public DemoDashboardLinkOptions() {
		buildCopyOnOpen = booleanParameter(id("Build_copy_on_open"), "Build copy on open",
				"Whether opening the demo link will create a dashboard copy for user with persistant results.");
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return ImmutableList.of(buildCopyOnOpen);
	}

	public boolean buildCopyOnOpen() {
		return buildCopyOnOpen.current();
	}

}
