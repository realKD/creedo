package de.unibonn.creedo.ui.indexpage;

import java.util.List;

import de.unibonn.creedo.webapp.CreedoSession;

/**
 * Can be added to {@link DefaultIndexPage} to provide dashboard links per user.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @version 0.1.2.1
 *
 */
public interface DashboardLinkProvider {

	/**
	 * 
	 * @return title for the list of dashboard links
	 */
	public String getTitle();

	public List<DashboardLinkEntry> getDashboardLinks(CreedoSession session);

}
