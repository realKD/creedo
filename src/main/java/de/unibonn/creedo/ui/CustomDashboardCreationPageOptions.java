package de.unibonn.creedo.ui;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;

/**
 * 
 * @author Mario Boley
 * 
 * @since 0.6.0
 * 
 * @version 0.6.0
 * 
 * @see CustomDashboardCreationPage
 *
 */
public class CustomDashboardCreationPageOptions implements ParameterContainer {

	private final SubCollectionParameter<String, Set<String>> systems;

	public CustomDashboardCreationPageOptions() {
		systems = Repositories.getIdentifierSetOfRepositoryParameter(Identifier.id("custom_dashboard_systems"),
				"Available systems",
				"The analysis systems from which a user can choose in the custom dashboard creation page.",
				ApplicationRepositories.MINING_SYSTEM_REPOSITORY);
	}
	
	public Collection<String> systems() {
		return systems.current();
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return ImmutableList.of(systems);
	}

}
