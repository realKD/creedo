package de.unibonn.creedo.common.parameters;

import java.util.List;
import java.util.Optional;

public class ParameterStateWithRangeInfo extends PlainParameterState {

	private final List<String> range;

	public ParameterStateWithRangeInfo(int depth, String type,
			String name, String description, boolean isActive, String dependsOnNotice,
			Optional<String> value, boolean isValid, String solutionHint,
			List<String> range, boolean hidden) {
		super(depth, type, name, description, isActive,
				dependsOnNotice, value, isValid, solutionHint, hidden);
		this.range = range;
	}

	public List<String> getRange() {
		return range;
	}

}
