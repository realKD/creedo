package de.unibonn.creedo.webapp.studies.rating;

public class RatingOption {

	private final String name;
	private final double value;

	public RatingOption(String name, double value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return this.name;
	}

	public double getValue() {
		return this.value;
	}
}
