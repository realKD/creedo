package de.unibonn.creedo.webapp.dashboard.mining;

import static de.unibonn.realkd.common.parameter.Parameters.subListParameter;
import static de.unibonn.realkd.common.base.Identifier.id;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.creedo.ui.core.UiRegister.IdGenerator;
import de.unibonn.creedo.webapp.viewmodels.DeveloperViewModel;
import de.unibonn.realkd.algorithms.MiningAlgorithm;
import de.unibonn.realkd.algorithms.MiningAlgorithmFactory;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.Parameters;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.discovery.DiscoveryProcess;
import de.unibonn.realkd.discovery.DiscoveryProcessState;

/**
 * Builder creating mining system instance configured as manual mining system
 * with a dialog for algorithm selection and parameter setting.
 *
 * @author Bjoern Jacobs
 * 
 * @since 0.1.0
 * 
 * @version 0.4.0
 * 
 */
public class ManualMiningSystemBuilder extends MiningSystemBuilder<ManualMiningSystemBuilder> {

	private final Parameter<Boolean> showLaunchDialog;

	private final Parameter<List<String>> userScripts;

	public ManualMiningSystemBuilder() {
		super();
		showLaunchDialog = Parameters.rangeEnumerableParameter(id("Show_dialog"), "Show dialog",
				"Determines whether launch dialog is shown before execution of mining algorithm ('false' only possible with single consistently initialized algorithm).",
				Boolean.class, () -> algorithms.current().size() == 1 ? ImmutableList.of(Boolean.TRUE, Boolean.FALSE)
						: ImmutableList.of(Boolean.TRUE),
				algorithms);
		Supplier<List<String>> allPages = () -> ApplicationRepositories.CONTENT_FOLDER_REPOSITORY.getAllEntries()
				.stream().filter(Repositories.getIdIsFilenameWithExtensionPredicate(Repositories.JS_FILE_EXTENSIONS))
				.map(e -> e.getId()).collect(toList());
		this.userScripts = subListParameter(id("User_scripts"), "User scripts",
				"Scripts to be injected into dashboard.", allPages);
		parameterContainer.addParameter(showLaunchDialog);
		parameterContainer.addParameter(userScripts);
	}

	@Override
	protected MiningSystem concreteBuild(IdGenerator idGenerator, DeveloperViewModel developerViewModel,
			Workspace dataWorkspace) {

		List<MiningAlgorithm> algorithms = new ArrayList<>();
		for (MiningAlgorithmFactory factory : getAlgorithmFactories()) {
			algorithms.add(factory.create(dataWorkspace));
		}

		DiscoveryProcessState state = dataWorkspace.get("$creedo_discovery_process", DiscoveryProcessState.class)
				.orElse(DiscoveryProcessState.discoveryProcessState("$creedo_discovery_process",
						"Creedo discovery process",
						"The iterative process in which the creedo users make their discoveries."));
		LOGGER.info("Deserialized " + (state.results().size() + state.candidates().size()) + " previous discoveries.");

		DiscoveryProcess discoveryProcess = new DiscoveryProcess(state);

		// return new MiningSystem(idGenerator, algorithms, discoveryProcess,
		// MineButtonStrategy.MANUALMINING, super.getRankerFactory()
		// .getRanker(dataWorkspace.getAllDatatables().get(0),
		// discoveryProcess),
		// showLaunchDialog.getCurrentValue());

		return new MiningSystem(idGenerator, algorithms, discoveryProcess, MineButtonStrategy.MANUALMINING,
				super.getRankerFactory().getRanker(dataWorkspace, discoveryProcess), showLaunchDialog.current(),
				helpPages.current(), defaultDataView.current(), usePCAasDefaultVisualization.current(),
				userScripts.current());

	}

	@Override
	public String toString() {
		return "Manual mining system";
	}

}
