package de.unibonn.creedo.webapp.handler;

import java.util.logging.Logger;

import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.google.common.collect.ImmutableSet;

import de.unibonn.creedo.WebConstants;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.ui.core.UiRegister.FrameCloser;

/**
 * A handler that internally uses a thread for registering Timeouts. Objects of
 * this class can register pings from the client window and monitors regularly
 * the time that passed since the last ping. If the time between the last ping
 * exceeds the allowed time-frame, the tear-down of the current analytics
 * dashboard is initiated.
 * 
 * @author Björn Jacobs
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2.1
 * 
 */
public class TimeoutHandler implements UiComponent {

	private static final Logger LOGGER = Logger.getLogger(TimeoutHandler.class
			.getName());

	private final TimeoutChecker timeoutChecker;

	private volatile long lastPingTimeStamp;

	private final Model model;

	private final int id;

	private final FrameCloser frameCloser;

	public TimeoutHandler(int id, FrameCloser frameCloser) {
		this.id = id;
		this.frameCloser = frameCloser;
		this.model = new BindingAwareModelMap();
		this.model.addAttribute("pingReceiverId", id);
		this.lastPingTimeStamp = System.currentTimeMillis();

		timeoutChecker = new TimeoutChecker();
		Thread timeoutCheckerThread = new Thread(timeoutChecker);
		timeoutCheckerThread.start();
	}

	public void ping() {
		LOGGER.finer("PING received!");
		lastPingTimeStamp = System.currentTimeMillis();
	}

	public void stop() {
		this.timeoutChecker.stop();
	}

	private class TimeoutChecker implements Runnable {

		private volatile boolean continueLoop = true;

		public TimeoutChecker() {
		}

		@Override
		public void run() {
			LOGGER.info("Starting Timeout thread");
			while (continueLoop) {
				try {
					Thread.sleep(WebConstants.TIMEOUT_CHECK_INTERVAL_MILLIS);
				} catch (InterruptedException e) {
					e.printStackTrace();
					continueLoop = false;
				}

				long currentTime = System.currentTimeMillis();
				long difference = currentTime - lastPingTimeStamp;

				LOGGER.finer("Timeout?: " + difference + " > "
						+ WebConstants.TIMEOUT_MILLIS + "?");

				if (difference > WebConstants.TIMEOUT_MILLIS) {
					// Tear-down AnalysisSession
					// creedoSession.tearDownDashboard();
					frameCloser.requestClose();

					// and stop this thread
					continueLoop = false;
				}
			}
			LOGGER.info("TimeoutHandler was requested to stop.");
		}

		public void stop() {
			continueLoop = false;
		}
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getView() {
		return "pinger.jsp";
	}

	@Override
	public Model getModel() {
		return model;
	}

	@Override
	public void tearDown() {
		stop();
		LOGGER.info("Stopped Timeout Checker.");
	}

	@Override
	public ImmutableSet<String> getOwnScriptFilenames() {
		return ImmutableSet.of("creedo-ping.js");
	}

}
