package de.unibonn.creedo.webapp;

import static de.unibonn.creedo.ui.indexpage.DashboardLinkEntry.dashboardLinkEntry;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.admin.options.Configurable;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.creedo.ui.indexpage.DashboardLinkBuilder;
import de.unibonn.creedo.ui.indexpage.DashboardLinkEntry;
import de.unibonn.creedo.ui.indexpage.DashboardLinkProvider;
import de.unibonn.realkd.common.base.ValidationException;

/**
 * Compiles the list of demo links available to a user.
 * 
 * @author Björn Jacobs
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2
 * 
 */
public class DemoProvider implements DashboardLinkProvider, Configurable<DemoProviderOptions> {

	public static final DemoProvider INSTANCE = new DemoProvider();

	public static final Logger LOGGER = Logger.getLogger(DemoProvider.class.getName());

	private DemoProvider() {
	}

	public List<DashboardLinkEntry> getDashboardLinks(CreedoSession session) {
		List<RepositoryEntry<String, DashboardLinkBuilder>> visible = ApplicationRepositories.DEMO_REPOSITORY
				.getAllEntries().stream().filter(x -> x.getContent().isVisibleTo(session.getUser()))
				.collect(Collectors.toList());
		List<DashboardLinkEntry> results = new ArrayList<>();
		for (RepositoryEntry<String, DashboardLinkBuilder> x : visible) {
			try {
				results.add(dashboardLinkEntry(x.getContent().build(session.getUser()), session));
			} catch (ValidationException e) {
				LOGGER.severe(String.format("Validation exception for '%s'; message: %s; hint: %s", x.getId(),
						e.getMessage(), e.hint()));
			}
		}
		return results;
	}

	@Override
	public String getTitle() {
		return getOptions().title();
	}

	@Override
	public DemoProviderOptions getDefaultOptions() {
		return new DemoProviderOptions();
	}

}
