package de.unibonn.creedo.webapp.patternviews;

import static de.unibonn.creedo.webapp.patternviews.PatternHtml.descriptionElement;
import static de.unibonn.creedo.webapp.patternviews.PatternHtml.explanationElement;
import static de.unibonn.creedo.webapp.patternviews.PatternHtml.tableCell;
import static java.lang.String.format;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import de.unibonn.realkd.common.measures.Measurement;
import de.unibonn.realkd.discovery.Discovery;
import de.unibonn.realkd.patterns.Pattern;

public class ResultPatternMapper implements PatternHTMLGenerator {

	public enum AnnotationVisibility {
		INVISIBLE {
			@Override
			void fillHtmlStringBuffer(StringBuilder builder, Discovery pattern) {
				;
			}
		},

		READONLY {
			@Override
			void fillHtmlStringBuffer(StringBuilder builder, Discovery pattern) {
				if (pattern.description().isEmpty()) {
					return;
				}
				builder.append("<textarea readonly class='annotation' row ='1' >");
				builder.append(pattern.description());
				builder.append("</textarea>");
			}
		},

		EDITABLE {
			@Override
			void fillHtmlStringBuffer(StringBuilder builder, Discovery pattern) {
				builder.append("<textarea class='annotation' row ='1' "
						+ "placeholder='Click to input annotation. Confirm with [ENTER]'>");
				builder.append(pattern.description());
				builder.append("</textarea>");
			}
		};

		abstract void fillHtmlStringBuffer(StringBuilder builder, Discovery pattern);
	}

	private final List<String> optionalActions;
	private final String htmlClass;
	private final AnnotationVisibility annotationVisibility;
	private final Function<Discovery, String> titleMapper;
	private final Function<Pattern<?>, String> descriptionMapper;
	
	private final int opendDetailedPatternViewActionId;
	private final int actionProviderId;
	private final int supportSetProviderId;

	public static String measurementTableRow(Measurement m) {
		return "<tr>" + tableCell(PatternHtml.documentedDiv(m.measure())) + tableCell(format("%.4f", m.value())) + "</tr>";
	}

	public static String measurementTable(Pattern<?> pattern) {
		StringBuilder result = new StringBuilder();
		result.append("<table style='width:100%'>");
		result.append(pattern.measures().stream().map(m->pattern.measurement(m).get()).map(ResultPatternMapper::measurementTableRow)
				.collect(Collectors.joining()));
		result.append("</table>");
		return result.toString();
	}

	public ResultPatternMapper(String htmlClass, List<String> optionalActions,
			AnnotationVisibility annotationVisibility, Function<Discovery, String> titleMapper,
			Function<Pattern<?>, String> descriptionMapper, int actionProviderId, int openDetailedPatternViewActionId, int supportSetProviderId) {
		this.optionalActions = optionalActions;
		this.annotationVisibility = annotationVisibility;
		this.titleMapper = titleMapper;
		this.htmlClass = htmlClass;
		this.descriptionMapper = descriptionMapper;
		this.opendDetailedPatternViewActionId=openDetailedPatternViewActionId;
		this.actionProviderId = actionProviderId;
		this.supportSetProviderId = supportSetProviderId;
	}

	@Override
	public String getHTML(HttpSession session, Discovery webPattern) {
		StringBuilder result = new StringBuilder();
		result.append("<div class=\"panel panel-default pattern " + htmlClass + "\" id=\"" + webPattern.number()
				+ "\" supportSetProviderId=\"" + supportSetProviderId + "\" actionProviderId=\""
				+ actionProviderId + "\" openDetailedViewActionId=\""
				+ opendDetailedPatternViewActionId + "\">");
		// header
		result.append(PatternHtml.titleDiv(titleMapper.apply(webPattern)));

		result.append(PatternHtml.actionsDiv(webPattern, optionalActions));

		// panel body
		result.append("<div class=\"panel-body\">");
		result.append("<div class=\"left pull-left\">");
		result.append(descriptionElement(descriptionMapper.apply(webPattern.content())));
		result.append(explanationElement(measurementTable(webPattern.content())));
		this.annotationVisibility.fillHtmlStringBuffer(result, webPattern);
		result.append("</div>");

		result.append(PatternHtml.illustrationDiv(session, webPattern.content()));

		result.append("<div class=\"clearfix\"></div>");

		// close panel-body and panel div tags
		result.append("</div></div>");
		return result.toString();
	}

}
