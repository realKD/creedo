package de.unibonn.creedo.webapp.patternviews;

import static de.unibonn.creedo.webapp.patternviews.PatternHtml.joinedWithLineBreaks;
import static de.unibonn.creedo.webapp.patternviews.PatternHtml.tableCell;
import static de.unibonn.realkd.util.Strings.recursivelyFormatted;
import static java.lang.String.format;
import static java.util.stream.Collectors.joining;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.discovery.Discovery;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.functional.CorrelationDescriptor;
import de.unibonn.realkd.patterns.functional.FunctionalPattern;
import de.unibonn.realkd.patterns.models.Model;
import de.unibonn.realkd.patterns.models.ModelParameter;
import de.unibonn.realkd.patterns.outlier.Outlier;
import de.unibonn.realkd.patterns.pmm.PureModelSubgroup;
import de.unibonn.realkd.patterns.subgroups.ControlledSubgroup;
import de.unibonn.realkd.patterns.subgroups.Subgroup;
import de.unibonn.realkd.util.Strings;

public class ResultPatternGenerator implements PatternHTMLGenerator {

	private static final List<String> ACTIONS = ImmutableList.of(PatternHtml.FILTER_DATA_ACTION_HTML,
			PatternHtml.OPEN_DETAILED_VIEW_ACTION_HTML, PatternHtml.DELETION_ACTION_HTML);

	private static String detailedExceptionalSubgroupTitle(Subgroup<?> subgroup) {
		String targetString = subgroup.targetAttributes().stream().map(a -> a.caption())
				.collect(Collectors.joining(",", "(", ")"));
		if (targetString.length() >= 15) {
			return "<strong>Excp. Sbgr.</strong> " + targetString;
		} else {
			return "<strong>Exceptional Subgroup</strong> " + targetString;
		}
	}

	private static String parameterTableRow(ModelParameter parameter, Model m1, Model m2) {
		return "<tr>" + PatternHtml.tableCell(PatternHtml.documentedDiv(parameter))
				+ PatternHtml.tableCell(recursivelyFormatted(m1.value(parameter).get(), "%.4f"))
				+ PatternHtml.tableCell(m2.value(parameter).map(v -> recursivelyFormatted(v, "%.4f")).orElse("--"))
				+ "</tr>";
	}

	public static String measurementTableRow(Measure f, Model m1, Model m2) {
		return "<tr>" + tableCell(PatternHtml.documentedDiv(f))
				+ tableCell(format("%.4f", m1.measurement(f).map(o -> o.value()).orElse(Double.NaN)))
				+ tableCell(format("%.4f", m2.measurement(f).map(o -> o.value()).orElse(Double.NaN))) + "</tr>";
	}

	private static String modelComparisonTable(String caption, Model m1, Model m2) {
		StringBuilder result = new StringBuilder();
		result.append("<table style='width:100%'>");
		result.append("<td colspan='3'><strong>" + caption + "</strong></td>");
		// result.append("<caption>" + caption + "</caption>");
		result.append(m1.parameters().stream().map(p -> parameterTableRow(p, m1, m2)).collect(joining("")));
		result.append(
				m1.measurements().stream().map(x -> measurementTableRow(x.measure(), m1, m2)).collect(joining("")));
		result.append("</table>");
		return result.toString();
	}

	private static String detailedSubgroupDescription(Subgroup<?> subgroup) {
		String selector = joinedWithLineBreaks(subgroup.extensionDescriptor().getElementsAsStringList());
		String targetModelCaption = subgroup.fittingAlgorithm().symbol()
				+ subgroup.targetAttributes().stream().map(a -> a.caption()).collect(joining(",", "(", ")"));
		String targetTable = modelComparisonTable(targetModelCaption, subgroup.localModel(), subgroup.referenceModel());
		String result = selector + "<hr>" + targetTable;
		if (subgroup instanceof ControlledSubgroup) {
			ControlledSubgroup<?, ?> controlledSubgroup = (ControlledSubgroup<?, ?>) subgroup;
			String controlModelCaption = controlledSubgroup.controlfittingAlgorithm().symbol() + controlledSubgroup
					.controlAttributes().stream().map(a -> a.caption()).collect(joining(",", "(", ")"));
			result += "<hr>" + modelComparisonTable(controlModelCaption, controlledSubgroup.localControlModel(),
					controlledSubgroup.referenceControlModel());
		}
		return result;
	}

	private final ResultPatternMapper emmGenerator;

	private final ResultPatternMapper pmmGenerator;

	private final ResultPatternMapper fpGenerator;

	private final PatternHTMLGenerator associationGenerator;

	private final ResultOutlierPatternGenerator outlierGenerator;

	private final ResultDefaultPatternGenerator defaultGenerator;

	public ResultPatternGenerator(int actionProviderId, int openDetailedPatternViewActionId, int supportSetProviderId) {
		outlierGenerator = new ResultOutlierPatternGenerator(ACTIONS, ResultPatternMapper.AnnotationVisibility.EDITABLE,
				actionProviderId, openDetailedPatternViewActionId, supportSetProviderId);
		associationGenerator = PatternHtml.associationDetailedHtmlGenerator(ACTIONS,
				ResultPatternMapper.AnnotationVisibility.EDITABLE, actionProviderId, openDetailedPatternViewActionId,
				supportSetProviderId);
		fpGenerator = new ResultPatternMapper("funcpattern", ImmutableList.of(PatternHtml.DELETION_ACTION_HTML),
				ResultPatternMapper.AnnotationVisibility.EDITABLE,
				d -> "<strong>Functional pattern</strong>" + ((CorrelationDescriptor) d.content().descriptor()).coDomain(),
				p -> ((CorrelationDescriptor) p.descriptor()).domain().stream().map(a -> a.caption())
						.collect(Collectors.joining(PatternHtml.LINEBREAK)),
				actionProviderId, openDetailedPatternViewActionId, supportSetProviderId);
		emmGenerator = new ResultPatternMapper("subgroup", ACTIONS, ResultPatternMapper.AnnotationVisibility.EDITABLE,
				webPattern -> detailedExceptionalSubgroupTitle((Subgroup<?>) webPattern.content().descriptor()),
				pattern -> detailedSubgroupDescription((Subgroup<?>) pattern.descriptor()), actionProviderId,
				openDetailedPatternViewActionId, supportSetProviderId);
		pmmGenerator = new ResultPatternMapper("subgroup", ACTIONS, ResultPatternMapper.AnnotationVisibility.EDITABLE,
				webPattern -> "<strong>Pure Subgroup</strong> ("
						+ Strings.chopped(((Subgroup<?>) webPattern.content().descriptor()).targetAttributes().stream()
								.map(a -> a.caption() + ",").collect(Collectors.joining()), 1)
						+ ")",
				pattern -> detailedSubgroupDescription(((Subgroup<?>) pattern.descriptor())), actionProviderId,
				openDetailedPatternViewActionId, supportSetProviderId);
		defaultGenerator = new ResultDefaultPatternGenerator(ACTIONS, ResultPatternMapper.AnnotationVisibility.EDITABLE,
				actionProviderId, openDetailedPatternViewActionId, supportSetProviderId);
	}

	@Override
	public String getHTML(HttpSession session, Discovery discovery) {
		if (discovery.content() instanceof Association) {
			return associationGenerator.getHTML(session, discovery);
		} else if (discovery.content() instanceof ExceptionalModelPattern) {
			return emmGenerator.getHTML(session, discovery);
		} else if (discovery.content() instanceof FunctionalPattern) {
			return fpGenerator.getHTML(session, discovery);
		} else if (discovery.content() instanceof PureModelSubgroup) {
			return pmmGenerator.getHTML(session, discovery);
		} else if (discovery.content() instanceof Outlier) {
			return outlierGenerator.getHTML(session, discovery);
		} else {
			return defaultGenerator.getHTML(session, discovery);
		}
	}

}
