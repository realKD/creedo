package de.unibonn.creedo.webapp.patternviews;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;

import javax.servlet.http.HttpSession;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.discovery.Discovery;
import de.unibonn.realkd.patterns.Frequency;
import de.unibonn.realkd.patterns.LocalPatternDescriptor;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.functional.CorrelationDescriptor;
import de.unibonn.realkd.patterns.functional.FunctionalPattern;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.patterns.outlier.Outlier;
import de.unibonn.realkd.patterns.pmm.PureModelSubgroup;
import de.unibonn.realkd.patterns.subgroups.Subgroup;

public class CandidatePatternGenerator implements PatternHTMLGenerator {

	private static final List<String> ACTIONS = ImmutableList.of(PatternHtml.FILTER_DATA_ACTION_HTML,
			PatternHtml.OPEN_DETAILED_VIEW_ACTION_HTML, PatternHtml.DELETION_ACTION_HTML);

	private final CandidateAssociationPatternMapper associationGenerator;

	private final DefaultCandidatePatternGenerator emmGenerator;

	private final DefaultCandidatePatternGenerator pmmGenerator;

	private final DefaultCandidatePatternGenerator fpGenerator;

	private final OutlierPatternGenerator outlierGenerator;

	private final DefaultCandidatePatternGenerator defaultGenerator;

	private static class SubgroupPatternToolTipMapper implements Function<Pattern<?>, String> {

		@Override
		public String apply(Pattern<?> pattern) {
			StringBuilder sb = new StringBuilder();
			LogicalDescriptor extensionDescriptor = ((Subgroup<?>) pattern.descriptor()).extensionDescriptor();
			for (int j = 0; j < extensionDescriptor.getElementsAsStringList().size(); j++) {
				sb.append(extensionDescriptor.getElementsAsStringList().get(j));
				sb.append("<br/>");
			}
			sb.append("-------------------------<br/>");

			for (Measure measure : pattern.measures()) {
				sb.append(measure.caption()).append(": ")
						.append(String.format(Locale.ENGLISH, "%.4f", pattern.value(measure))).append("<br/>");
			}

			return sb.toString();
		}

	}

	private static class CandidatePureModelExplanationMapper implements Function<Pattern<?>, List<String>> {

		@Override
		public List<String> apply(Pattern<?> pattern) {
			checkArgument(pattern instanceof PureModelSubgroup, "Pattern must be pmm pattern");
			checkArgument(pattern.descriptor() instanceof LocalPatternDescriptor,
					"Pattern descriptor must describe sub population of data.");
			List<String> result = new ArrayList<>();
			Measure deviationIntMeasure = ((PureModelSubgroup) pattern).purityGainMeasure();
			result.add("has <b>pure distribution of " + getTargetsString(pattern) + "</b> " + "("
					+ deviationIntMeasure.caption() + " "
					+ String.format(Locale.ENGLISH, "%.3f", pattern.value(deviationIntMeasure)) + ")");

			result.add(
					"and <b>contains</b> " + ((LocalPatternDescriptor) pattern.descriptor()).supportSet().size()
							+ " data points"
							+ (pattern.hasMeasure(Frequency.FREQUENCY)
									? String.format(Locale.ENGLISH, " (freq. %.3f)", pattern.value(Frequency.FREQUENCY))
									: ""));
			return result;
		}

		private String getTargetsString(Pattern<?> pattern) {
			StringBuilder sb = new StringBuilder();
			for (Attribute<?> attribute : ((PureModelSubgroup) pattern).descriptor().targetAttributes()) {
				sb.append(attribute.caption() + ", ");
			}
			return sb.substring(0, sb.length() - 2);
		}

	}

	private static class CandidateExceptionalModelExplanationMapper implements Function<Pattern<?>, List<String>> {

		@Override
		public List<String> apply(Pattern<?> pattern) {
			checkArgument(pattern instanceof ExceptionalModelPattern, "Pattern must be EMM pattern");
			checkArgument(pattern.descriptor() instanceof LocalPatternDescriptor,
					"Pattern descriptor must describe sub population of data.");
			List<String> result = new ArrayList<>();
			Measure deviationIntMeasure = ((ExceptionalModelPattern) pattern).getDeviationMeasure();
			result.add("has <b>unusual distribution of " + getTargetsString(pattern) + "</b> "
			// + "(dev. "
					+ "(" + deviationIntMeasure.caption() + " "
					+ String.format(Locale.ENGLISH, "%.3f", pattern.value(deviationIntMeasure))
					// ((ExceptionalModelPattern) pattern).getModelDeviation())
					+ ")");

			result.add(
					"and <b>contains</b> " + ((LocalPatternDescriptor) pattern.descriptor()).supportSet().size()
							+ " data points"
							+ (pattern.hasMeasure(Frequency.FREQUENCY)
									? String.format(Locale.ENGLISH, " (freq. %.3f)", pattern.value(Frequency.FREQUENCY))
									: ""));
			return result;
		}

		private String getTargetsString(Pattern<?> pattern) {
			StringBuilder sb = new StringBuilder();
			for (Attribute<?> attribute : ((ExceptionalModelPattern) pattern).descriptor().targetAttributes()) {
				sb.append(attribute.caption() + ", ");
			}
			return sb.substring(0, sb.length() - 2);
		}

	}

	private static class DefaultPatternToolTipMapper implements Function<Pattern<?>, String> {
		@Override
		public String apply(Pattern<?> pattern) {
			StringBuffer res = new StringBuffer();
			if (pattern.descriptor() instanceof LocalPatternDescriptor) {
				LocalPatternDescriptor subPopulationDescriptor = (LocalPatternDescriptor) pattern.descriptor();
				for (Integer index : subPopulationDescriptor.supportSet()) {
					res.append("<br/>");
					res.append(subPopulationDescriptor.population().objectName(index));
				}
			}
			return res.toString();
		}
	}

	private static class DefaultPatternDescriptionMapper implements Function<Pattern<?>, List<String>> {

		@Override
		public List<String> apply(Pattern<?> pattern) {
			if (!(pattern.descriptor() instanceof LocalPatternDescriptor)) {
				return Arrays.asList("No description available.");
			}

			LocalPatternDescriptor subPopulationDescriptor = (LocalPatternDescriptor) pattern.descriptor();
			List<String> res = new ArrayList<>(subPopulationDescriptor.supportSet().size());
			for (Integer index : subPopulationDescriptor.supportSet()) {
				res.add(subPopulationDescriptor.population().objectName(index));
			}

			return res;
		}

	}

	public CandidatePatternGenerator(int actionProviderId, int openDetailedPatternViewActionId,
			int supportSetProviderId) {
		this.associationGenerator = new CandidateAssociationPatternMapper(ACTIONS, actionProviderId,
				openDetailedPatternViewActionId, supportSetProviderId);
		this.emmGenerator = new DefaultCandidatePatternGenerator(ACTIONS, webPattern -> "Subgroup of Rows with",
				pattern -> ((ExceptionalModelPattern) pattern).descriptor().extensionDescriptor()
						.getElementsAsStringList(),
				"subgroup", new SubgroupPatternToolTipMapper(), new CandidateExceptionalModelExplanationMapper(),
				actionProviderId, openDetailedPatternViewActionId, supportSetProviderId);
		this.pmmGenerator = new DefaultCandidatePatternGenerator(ACTIONS, webPattern -> "Subgroup of Rows with",
				pattern -> ((PureModelSubgroup) pattern).descriptor().extensionDescriptor().getElementsAsStringList(),
				"subgroup", new SubgroupPatternToolTipMapper(), new CandidatePureModelExplanationMapper(),
				actionProviderId, openDetailedPatternViewActionId, supportSetProviderId);
		this.fpGenerator = new DefaultCandidatePatternGenerator(ImmutableList.of(PatternHtml.DELETION_ACTION_HTML),
				webPattern -> "Attribute Set",
				pattern -> ((FunctionalPattern) pattern).descriptor().domain().stream().map(a -> a.caption())
						.collect(toList()),
				"funcpattern", new DefaultPatternToolTipMapper(),
				p -> ImmutableList.of(
						"provides a lot of <b>information about " + ((CorrelationDescriptor) p.descriptor()).coDomain()
								+ "</b> (" + ((FunctionalPattern) p).functionalityMeasure().caption()
								+ String.format(" %.3f)", p.value(((FunctionalPattern) p).functionalityMeasure()))),
				actionProviderId, openDetailedPatternViewActionId, supportSetProviderId);
		this.outlierGenerator = new OutlierPatternGenerator(ACTIONS, actionProviderId, openDetailedPatternViewActionId,
				supportSetProviderId);
		this.defaultGenerator = new DefaultCandidatePatternGenerator(ACTIONS, webPattern -> "Pattern",
				new DefaultPatternDescriptionMapper(), "generic-pattern", new DefaultPatternToolTipMapper(),
				pattern -> ImmutableList.of(), actionProviderId, openDetailedPatternViewActionId, supportSetProviderId);
	}

	@Override
	public String getHTML(HttpSession session, Discovery webPattern) {
		if (webPattern.content() instanceof Association) {
			return associationGenerator.getHTML(session, webPattern);
		} else if (webPattern.content() instanceof ExceptionalModelPattern) {
			return emmGenerator.getHTML(session, webPattern);
		} else if (webPattern.content() instanceof PureModelSubgroup) {
			return pmmGenerator.getHTML(session, webPattern);
		} else if (webPattern.content() instanceof FunctionalPattern) {
			return fpGenerator.getHTML(session, webPattern);
		} else if (webPattern.content() instanceof Outlier) {
			return outlierGenerator.getHTML(session, webPattern);
		} else {
			return defaultGenerator.getHTML(session, webPattern);
		}
	}
}
