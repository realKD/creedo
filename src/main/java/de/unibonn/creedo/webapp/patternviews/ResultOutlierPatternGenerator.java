package de.unibonn.creedo.webapp.patternviews;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.LocalPatternDescriptor;
import de.unibonn.realkd.patterns.TableSubspaceDescriptor;

public class ResultOutlierPatternGenerator extends ResultPatternMapper {

	public ResultOutlierPatternGenerator(List<String> optionalActions, AnnotationVisibility annotationVisibility,
			int actionProviderId, int openDetailedPatternViewAction, int supportSetProviderId) {
		super("generic-pattern", optionalActions, annotationVisibility, webPattern -> "Outlier Pattern",
				new ResultOutlierDescriptionMapping(), actionProviderId, openDetailedPatternViewAction,
				supportSetProviderId);
	}

	private static class ResultOutlierDescriptionMapping implements Function<Pattern<?>, String> {

		@Override
		public String apply(Pattern<?> pattern) {
			checkArgument(pattern.descriptor() instanceof LocalPatternDescriptor,
					"Descriptor must describe sub population.");
			checkArgument(pattern.descriptor() instanceof TableSubspaceDescriptor,
					"Descriptor must describe table subspace.");
			LocalPatternDescriptor subPopulationDescriptor = (LocalPatternDescriptor) pattern.descriptor();
			List<String> res = new ArrayList<>(subPopulationDescriptor.supportSet().size());
			res.add("The rows");
			for (Integer index : subPopulationDescriptor.supportSet()) {
				res.add(subPopulationDescriptor.population().objectName(index));
			}
			if (!((TableSubspaceDescriptor) pattern.descriptor()).getReferencedAttributes().isEmpty()) {
				res.add("behave annormaly in terms of " + getAttributesDescriptionSubstring(pattern) + ".");
			}
			return PatternHtml.joinedWithLineBreaks(res);
		}

		private String getAttributesDescriptionSubstring(Pattern<?> pattern) {
			StringBuffer resultBuffer = new StringBuffer();
			Iterator<Attribute<?>> attributes = ((TableSubspaceDescriptor) pattern.descriptor())
					.getReferencedAttributes().iterator();
			while (attributes.hasNext()) {
				resultBuffer.append("<strong>" + attributes.next().caption() + "</strong>");
				if (attributes.hasNext()) {
					resultBuffer.append(", ");
				}
			}
			return resultBuffer.toString();
		}

	}

}
