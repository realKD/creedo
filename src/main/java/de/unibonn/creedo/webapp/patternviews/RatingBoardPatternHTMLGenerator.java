package de.unibonn.creedo.webapp.patternviews;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.webapp.patternviews.ResultPatternMapper.AnnotationVisibility;
import de.unibonn.realkd.discovery.Discovery;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.patternset.PatternSet;
import de.unibonn.realkd.patterns.pmm.PureModelSubgroup;
import de.unibonn.realkd.patterns.subgroups.Subgroup;
import de.unibonn.realkd.util.Strings;

public class RatingBoardPatternHTMLGenerator implements PatternHTMLGenerator {

	public static final String SELECT_ACTION_HTML = "<span class=\"selectable\"/>";

	private static List<String> ACTIONS = new ArrayList<String>();

	static {
		// ACTIONS.add(GeneralPattenGenerator.DELETION_ACTION_HTML);
		ACTIONS.add(SELECT_ACTION_HTML);
		ACTIONS.add(PatternHtml.FILTER_DATA_ACTION_HTML);
		ACTIONS.add(PatternHtml.OPEN_DETAILED_VIEW_ACTION_HTML);
	}

	private final ResultDefaultPatternGenerator defaultGenerator;

	private final ResultPatternMapper emmGenerator;

	private final ResultPatternMapper pmmGenerator;

	private final PatternHTMLGenerator associationGenerator;

	private final ResultPatternSetMapper patternSetGenerator;

	public RatingBoardPatternHTMLGenerator(int actionProviderId, int openDetailedPatternViewActionId,
			int supportSetProviderId) {
		emmGenerator = new ResultPatternMapper("subgroup", ACTIONS, AnnotationVisibility.READONLY,
				webPattern -> "<strong>Exceptional Subgroup</strong> ("
						+ Strings.chopped(((Subgroup<?>) webPattern.content().descriptor()).targetAttributes()
								.stream().map(a -> a.caption() + ",").collect(Collectors.joining()), 1)
						+ ")",
				pattern -> PatternHtml.joinedWithLineBreaks(
						((Subgroup<?>) pattern.descriptor()).extensionDescriptor().getElementsAsStringList()),
				actionProviderId, openDetailedPatternViewActionId, supportSetProviderId);
		associationGenerator = PatternHtml.associationDetailedHtmlGenerator(ACTIONS, AnnotationVisibility.READONLY,
				actionProviderId, openDetailedPatternViewActionId, supportSetProviderId);
		pmmGenerator = new ResultPatternMapper("subgroup", ACTIONS, AnnotationVisibility.READONLY,
				webPattern -> "<strong>Pure Subgroup</strong> ("
						+ Strings.chopped(((Subgroup<?>) webPattern.content().descriptor()).targetAttributes()
								.stream().map(a -> a.caption() + ",").collect(Collectors.joining()), 1)
						+ ")",
				pattern -> PatternHtml.joinedWithLineBreaks(
						((Subgroup<?>) pattern.descriptor()).extensionDescriptor().getElementsAsStringList()),
				actionProviderId, openDetailedPatternViewActionId, supportSetProviderId);
		defaultGenerator = new ResultDefaultPatternGenerator(ACTIONS, AnnotationVisibility.READONLY,
				actionProviderId, openDetailedPatternViewActionId, supportSetProviderId);
		patternSetGenerator = new ResultPatternSetMapper(ACTIONS, actionProviderId, openDetailedPatternViewActionId,
				supportSetProviderId);
	}

	@Override
	public String getHTML(HttpSession session, Discovery webPattern) {
		if (webPattern.content() instanceof Association) {
			return associationGenerator.getHTML(session, webPattern);
		} else if (webPattern.content() instanceof ExceptionalModelPattern) {
			return emmGenerator.getHTML(session, webPattern);
		} else if (webPattern.content() instanceof PureModelSubgroup) {
			return pmmGenerator.getHTML(session, webPattern);
		} else if (webPattern.content() instanceof PatternSet) {
			return patternSetGenerator.getHTML(session, webPattern);
		} else {
			return defaultGenerator.getHTML(session, webPattern);
		}
	}

}
