package de.unibonn.creedo.webapp.patternviews;

import static de.unibonn.creedo.webapp.utils.ServerVisualizationTools.serveVisualizationAndGetUrl;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.webapp.patternviews.ResultPatternMapper.AnnotationVisibility;
import de.unibonn.realkd.common.base.Documented;
import de.unibonn.realkd.discovery.Discovery;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.visualization.pattern.PatternVisualization;
import de.unibonn.realkd.visualization.pattern.RegisteredPatternVisualization;

public class PatternHtml {

	/**
	 * 
	 */
	private static final String EXPLANATION_CLOSE_TAG = "</div>";

	/**
	 * 
	 */
	private static final String EXPLANATION_OPEN_TAG = "<div class='explanation'>";

	/**
	 * 
	 */
	public static final String LINEBREAK = "<br/>";

	/**
	 * 
	 */
	private static final String DESCRIPTION_CLOSE_TAG = EXPLANATION_CLOSE_TAG;

	/**
	 * 
	 */
	private static final String DESCRIPTION_OPEN_TAG = "<div class='description'>";

	public static final String FILTER_DATA_ACTION_HTML = "<a href=\"#\" title='Filter data view' class=\"btn-toggle-support-set\"><span class=\"glyphicon glyphicon-screenshot\"></span></a>";

	public static final String OPEN_DETAILED_VIEW_ACTION_HTML = "<a href=\"#\" title='Detailed view' class='detailed-view'><span class=\"glyphicon glyphicon-search\"></span></a>";

	/**
	 * This string has some leading space in order to separate this action from
	 * actions from the left. Consequently, it should always be added as last
	 * optional action.
	 */
	public static final String DELETION_ACTION_HTML = "&nbsp;&nbsp;&nbsp;<a href=\"#\" title='Discard' class=\"btn-remove-pattern\"><span class=\"glyphicon glyphicon-remove\"></span></a>";

	private PatternHtml(List<String> optionalActions) {
		;
	}

	public static String actionsDiv(Discovery webPattern, List<String> optionalActions) {
		StringBuilder result = new StringBuilder();
		result.append("<div class=\"actions pull-right\">");
		for (String action : optionalActions) {
			result.append(action);
		}
		result.append("</div><div class=\"clearfix\"></div></div>");
		return result.toString();
	}

	public static String illustrationDiv(HttpSession session, Pattern<?> pattern) {
		StringBuilder result = new StringBuilder();
		result.append("<div class='visualization'>");

		for (PatternVisualization visualization : RegisteredPatternVisualization.values()) {
			if (visualization.isApplicable(pattern)) {
				result.append(imageTag(visualization, pattern, session));
			}
		}

		result.append(DESCRIPTION_CLOSE_TAG);
		return result.toString();
	}

	private static String imageTag(PatternVisualization pv, Pattern<?> pattern, HttpSession session) {
		return "<img src='" + serveVisualizationAndGetUrl(pv, pattern, session) + "' /> ";
	}

	public static PatternHTMLGenerator associationDetailedHtmlGenerator(List<String> optionalActions,
			AnnotationVisibility annotationVisibility, int actionProviderId, int openDetailedPatternViewActionId,
			int supportSetProviderId) {
		return new ResultPatternMapper("association", optionalActions, annotationVisibility,
				webPattern -> webPattern.content().hasMeasure(QualityMeasureId.LIFT)
						? "<strong>Positively Associated Attribute Values</strong>"
						: "<strong>Negatively Associated Attribute Values</strong>",
				p -> joinedWithLineBreaks(LogicalDescriptorPatternToAnnotatedDescriptorMap.INSTANCE.apply(p)),
				actionProviderId, openDetailedPatternViewActionId, supportSetProviderId);
	}

	// public static String descriptionElementFromContentLines(List<String>
	// descriptorList) {
	// return descriptorList.stream()
	// .collect(Collectors.joining(LINEBREAK, DESCRIPTION_OPEN_TAG,
	// DESCRIPTION_CLOSE_TAG));
	// }

	public static String descriptionElement(String content) {
		return DESCRIPTION_OPEN_TAG + content + DESCRIPTION_CLOSE_TAG;
	}

	public static String joinedWithLineBreaks(List<String> lines) {
		return lines.stream().collect(Collectors.joining(LINEBREAK));
	}

	public static String titleDiv(String titleContent) {
		return "<div class=\"panel-heading\"><div class=\"title pull-left\">" + titleContent + EXPLANATION_CLOSE_TAG;
	}

	public static String explanationElement(String content) {
		StringBuilder sb = new StringBuilder();
		sb.append(EXPLANATION_OPEN_TAG);
		sb.append(content);
		sb.append(EXPLANATION_CLOSE_TAG);
		return sb.toString();
	}

	public static String tableCell(String content) {
		return "<td>" + content + "</td>";
	}

	public static String documentedDiv(Documented d) {
		return "<div title='" + d.caption() + " - " + d.description() + "'>" + d.symbol() + "</div>";
	}

}