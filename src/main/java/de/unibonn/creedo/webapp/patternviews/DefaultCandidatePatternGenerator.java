package de.unibonn.creedo.webapp.patternviews;

import java.util.List;
import java.util.function.Function;

import javax.servlet.http.HttpSession;

import de.unibonn.realkd.discovery.Discovery;
import de.unibonn.realkd.patterns.Pattern;

public class DefaultCandidatePatternGenerator implements PatternHTMLGenerator {

	private final Function<Discovery, String> titleMapper;

	private final Function<Pattern<?>, List<String>> patternToDescriptorMapper;

	private final String htmlClass;

	private final Function<Pattern<?>, String> toolTipMapper;

	private final Function<Pattern<?>, List<String>> explanationMapper;

	private final List<String> optionalActions;

	private final int openDetailedPatternViewActionId;

	private final int actionProviderId;

	private final int supportSetProviderId;

	public DefaultCandidatePatternGenerator(List<String> optionalActions, Function<Discovery, String> titleMapper,
			Function<Pattern<?>, List<String>> patternToDescriptorMapper, String htmlClass,
			Function<Pattern<?>, String> toolTipMapper, Function<Pattern<?>, List<String>> explanationMapper,
			int actionProviderId, int openDetailedPatternViewActionId, int supportSetProviderId) {
		this.optionalActions = optionalActions;
		this.titleMapper = titleMapper;
		this.toolTipMapper = toolTipMapper;
		this.htmlClass = htmlClass;
		this.patternToDescriptorMapper = patternToDescriptorMapper;
		this.explanationMapper = explanationMapper;
		this.openDetailedPatternViewActionId = openDetailedPatternViewActionId;
		this.actionProviderId = actionProviderId;
		this.supportSetProviderId = supportSetProviderId;
	}

	private String getTitleDiv(String title) {
		return "<div class=\"panel-heading\"><div class=\"title pull-left\">" + title + "</div>";
	}

	@Override
	public String getHTML(HttpSession session, Discovery webPattern) {
		StringBuilder result = new StringBuilder();
		result.append("<div class=\"panel panel-default pattern " + htmlClass + "\" "
				+ "data-toggle=\"tooltip\" data-html=\"true\" data-placement=\"left\" id=\"" + webPattern.number()
				+ "\" title=\"" + toolTipMapper.apply(webPattern.content()) + "\" supportSetProviderId=\""
				+ supportSetProviderId + "\" actionProviderId=\"" + actionProviderId + "\" openDetailedViewActionId=\""
				+ openDetailedPatternViewActionId + "\">");
		// header
		result.append(getTitleDiv(titleMapper.apply(webPattern)));
		result.append(PatternHtml.actionsDiv(webPattern, optionalActions));

		// panel body
		result.append("<div class=\"panel-body\">");

		// description
		result.append(getDescription(webPattern.content()));

		// explanation
		result.append(getExplanation(webPattern.content()));

		// visualization
		result.append(PatternHtml.illustrationDiv(session, webPattern.content()));

		// close panel-body and panel div tags
		result.append("</div></div>");

		return result.toString();
	}

	private String getDescription(Pattern<?> pattern) {
		// List<String> descriptorList = getDescriptorElements(pattern);
		List<String> descriptorList = patternToDescriptorMapper.apply(pattern);

		StringBuilder sb = new StringBuilder();
		sb.append("<div class='description'>");

		if (!descriptorList.isEmpty()) {
			sb.append(descriptorList.get(0));
			for (int dscrpCount = 1; dscrpCount < VISIBLE_DESCRIPTOR_NUMBER
					&& dscrpCount < descriptorList.size(); dscrpCount++) {
				sb.append("<br/>");
				sb.append(descriptorList.get(dscrpCount));
			}
		}
		if (descriptorList.size() > VISIBLE_DESCRIPTOR_NUMBER) {
			sb.append("<span id = \"hasMore\"><br/>... </span>");
		}

		sb.append("</div>");
		return sb.toString();
	}

	private final String getExplanation(Pattern<?> pattern) {
		StringBuilder sb = new StringBuilder();
		sb.append("<div class='explanation'>");
		for (String element : explanationMapper.apply(pattern)) {
			sb.append(element);
			sb.append("<br/>");
		}
		sb.append("</div>");
		return sb.toString();
	}

}
