package de.unibonn.creedo.webapp.patternviews;

import javax.servlet.http.HttpSession;

import de.unibonn.realkd.discovery.Discovery;

public interface PatternHTMLGenerator {

	public String getHTML(HttpSession session, Discovery webPattern);

	static int VISIBLE_DESCRIPTOR_NUMBER = 3;

}
