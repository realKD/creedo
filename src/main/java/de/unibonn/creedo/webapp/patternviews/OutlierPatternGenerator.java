package de.unibonn.creedo.webapp.patternviews;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.LocalPatternDescriptor;

public class OutlierPatternGenerator extends DefaultCandidatePatternGenerator {

	private static class OutlierTooltipMapper implements Function<Pattern<?>, String> {
		@Override
		public String apply(Pattern<?> pattern) {
			checkArgument(pattern.descriptor() instanceof LocalPatternDescriptor,
					"Pattern descriptor must describe sub population of data.");
			StringBuffer res = new StringBuffer();
			LocalPatternDescriptor subPopulation = (LocalPatternDescriptor) pattern.descriptor();
			for (Integer index : subPopulation.supportSet()) {
				res.append("<br/>");
				res.append(subPopulation.population().objectName(index));
			}
			return res.toString();
		}
	}

	private static class OutlierDescriptionMapper implements Function<Pattern<?>, List<String>> {
		@Override
		public List<String> apply(Pattern<?> pattern) {
			checkArgument(pattern.descriptor() instanceof LocalPatternDescriptor,
					"Pattern descriptor must describe subpopulation of data.");
			LocalPatternDescriptor subPopulation = (LocalPatternDescriptor) pattern.descriptor();
			List<String> res = new ArrayList<>(subPopulation.supportSet().size());
			for (Integer index : subPopulation.supportSet()) {
				res.add(subPopulation.population().objectName(index));
			}
			return res;
		}
	}

	public OutlierPatternGenerator(List<String> optionalActions, int actionProviderId,
			int openDetailedPatternViewActionId, int supportSetProviderId) {
		super(optionalActions, webPattern -> "Outlier Pattern", new OutlierDescriptionMapper(), "generic-pattern",
				new OutlierTooltipMapper(), pattern -> ImmutableList.of(), actionProviderId,
				openDetailedPatternViewActionId, supportSetProviderId);
	}

}
