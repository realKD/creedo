package de.unibonn.creedo.webapp.patternviews;

import de.unibonn.creedo.webapp.patternviews.ResultPatternMapper.AnnotationVisibility;
import de.unibonn.realkd.discovery.Discovery;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.patternset.PatternSet;
import de.unibonn.realkd.patterns.pmm.PureModelSubgroup;
import de.unibonn.realkd.patterns.subgroups.Subgroup;
import de.unibonn.realkd.util.Strings;

import javax.servlet.http.HttpSession;

import static de.unibonn.creedo.webapp.patternviews.PatternHtml.joinedWithLineBreaks;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Bo Kang
 * 
 * @since 0.3.0
 * 
 * @version 0.5.0
 * 
 */
public class ResultPatternSetMapper implements PatternHTMLGenerator {

	private static class PatternWithinPatternSetHtmGenerator implements PatternHTMLGenerator {

		private static List<String> ACTIONS = new ArrayList<String>();

		private final ResultDefaultPatternGenerator defaultGenerator;

		private final ResultPatternMapper emmGenerator;

		// TODO why do these mappers not reduce the standard results mappers?
		private final ResultPatternMapper pmmGenerator;

		private final PatternHTMLGenerator associationGenerator;

		private final ResultPatternSetMapper patternSetGenerator;

		private PatternWithinPatternSetHtmGenerator(int actionProviderId, int openDetailedPatternViewActionId,
				int supportSetProviderId) {
			associationGenerator = PatternHtml.associationDetailedHtmlGenerator(ACTIONS,
					ResultPatternMapper.AnnotationVisibility.READONLY, actionProviderId,
					openDetailedPatternViewActionId, supportSetProviderId);
			defaultGenerator = new ResultDefaultPatternGenerator(ACTIONS,
					ResultPatternMapper.AnnotationVisibility.READONLY, actionProviderId,
					openDetailedPatternViewActionId, supportSetProviderId);
			patternSetGenerator = new ResultPatternSetMapper(ACTIONS, actionProviderId, openDetailedPatternViewActionId,
					supportSetProviderId);
			emmGenerator = new ResultPatternMapper("subgroup", ACTIONS, AnnotationVisibility.READONLY,
					webPattern -> "<strong>Exceptional Subgroup</strong> ("
							+ Strings.chopped(((Subgroup<?>) webPattern.content().descriptor()).targetAttributes()
									.stream().map(a -> a.caption() + ",").collect(Collectors.joining()), 1)
							+ ")",
					pattern -> joinedWithLineBreaks(
							((Subgroup<?>) pattern.descriptor()).extensionDescriptor().getElementsAsStringList()),
					actionProviderId, openDetailedPatternViewActionId, supportSetProviderId);
			pmmGenerator = new ResultPatternMapper("subgroup", ACTIONS, AnnotationVisibility.READONLY,
					webPattern -> "<strong>Pure Subgroup</strong> ("
							+ Strings.chopped(((Subgroup<?>) webPattern.content().descriptor()).targetAttributes()
									.stream().map(a -> a.caption() + ",").collect(Collectors.joining()), 1)
							+ ")",
					pattern -> joinedWithLineBreaks(
							((Subgroup<?>) pattern.descriptor()).extensionDescriptor().getElementsAsStringList()),
					actionProviderId, openDetailedPatternViewActionId, supportSetProviderId);
		}

		@Override
		public String getHTML(HttpSession session, Discovery webPattern) {
			if (webPattern.content() instanceof Association) {
				return associationGenerator.getHTML(session, webPattern);
			} else if (webPattern.content() instanceof ExceptionalModelPattern) {
				return emmGenerator.getHTML(session, webPattern);
			} else if (webPattern.content() instanceof PureModelSubgroup) {
				return pmmGenerator.getHTML(session, webPattern);
			} else if (webPattern.content() instanceof PatternSet) {
				return patternSetGenerator.getHTML(session, webPattern);
			} else {
				return defaultGenerator.getHTML(session, webPattern);
			}
		}
	}

	private final List<String> optionalActions;

	private final PatternWithinPatternSetHtmGenerator patternHtmlGenerator;

	private final int openDetailedPatternViewActionId;

	private final int actionProviderId;

	private final int supportSetProviderId;

	public ResultPatternSetMapper(List<String> optionalActions, int actionProviderId,
			int openDetailedPatternViewActionId, int supportSetProviderId) {
		this.openDetailedPatternViewActionId = openDetailedPatternViewActionId;
		this.actionProviderId = actionProviderId;
		this.supportSetProviderId = supportSetProviderId;
		this.optionalActions = optionalActions;
		this.patternHtmlGenerator = new PatternWithinPatternSetHtmGenerator(actionProviderId,
				openDetailedPatternViewActionId, supportSetProviderId);
	}

	@Override
	public String getHTML(HttpSession session, Discovery webPattern) {
		StringBuilder buffer = new StringBuilder();
		if (!(webPattern.content() instanceof PatternSet)) {
			throw new IllegalArgumentException("can only be applied to PatternSet");
		}
		Set<Pattern<?>> patternSet = ((PatternSet) webPattern.content()).descriptor().getPatterns();

		buffer.append("<div class=\"panel panel-default pattern " + "pattern-set" + "\" id=\"" + webPattern.number()
				+ "\" supportsetproviderid=\"" + supportSetProviderId + "\" actionProviderId=\"" + actionProviderId
				+ "\" openDetailedViewActionId=\"" + openDetailedPatternViewActionId + "\">");
		buffer.append("<div class=\"panel-heading\"><div class=\"title pull-left\">" + "Pattern Set" + "</div>");
		buffer.append(PatternHtml.actionsDiv(webPattern, optionalActions));
		int i = 0;
		for (Pattern<?> pattern : patternSet) {
			i++;
			buffer.append(patternHtmlGenerator.getHTML(session,
					new Discovery(webPattern.identifier() + "$" + i, "", "", webPattern.number(), pattern)));
			// TODO can not meaningfully provide number here; either have html
			// generator that maps pattern only (and not requiring discovery
			// wrapper), which is preferred; or eliminate concept of numbers,
			// which makes only sense within discovery process
		}

		buffer.append("</div>");
		return buffer.toString();
	}
}
