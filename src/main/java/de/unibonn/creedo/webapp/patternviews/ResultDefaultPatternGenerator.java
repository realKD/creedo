package de.unibonn.creedo.webapp.patternviews;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.LocalPatternDescriptor;
import de.unibonn.realkd.patterns.TableSubspaceDescriptor;

public class ResultDefaultPatternGenerator extends ResultPatternMapper {

	public ResultDefaultPatternGenerator(List<String> optionalActions, AnnotationVisibility annotationVisibility,
			int actionProviderId, int openDetailedPatternViewAction, int supportSetProviderId) {
		super("generic-pattern", optionalActions, annotationVisibility, webPattern -> "Pattern",
				new ResultDefaultPatternDescriptionMapper(), actionProviderId, openDetailedPatternViewAction,
				supportSetProviderId);
	}

	private static class ResultDefaultPatternDescriptionMapper implements Function<Pattern<?>, String> {

		@Override
		public String apply(Pattern<?> pattern) {
			if (pattern.descriptor() instanceof LocalPatternDescriptor) {
				return PatternHtml
						.joinedWithLineBreaks(getDescriptionElements((LocalPatternDescriptor) pattern.descriptor()));
			}
			return "No description available.";
		}

		private List<String> getDescriptionElements(LocalPatternDescriptor descriptor) {
			List<String> res = new ArrayList<>(descriptor.supportSet().size());

			res.add("The rows");
			for (Integer index : descriptor.supportSet()) {
				res.add(descriptor.population().objectName(index));
			}
			if ((descriptor instanceof TableSubspaceDescriptor)
					&& !((TableSubspaceDescriptor) descriptor).getReferencedAttributes().isEmpty()) {
				res.add("have a notable behavior in term of " + getAttributesDescriptionSubstring(
						((TableSubspaceDescriptor) descriptor).getReferencedAttributes()) + ".");
			} else {
				res.add("stick out.");
			}

			return res;
		}

		private String getAttributesDescriptionSubstring(List<Attribute<?>> attributes) {
			StringBuffer resultBuffer = new StringBuffer();
			Iterator<Attribute<?>> attributeIterator = attributes.iterator();
			while (attributeIterator.hasNext()) {
				resultBuffer.append("<strong>" + attributeIterator.next().caption() + "</strong>");
				if (attributeIterator.hasNext()) {
					resultBuffer.append(", ");
				}
			}
			return resultBuffer.toString();
		}

	}

}
