package de.unibonn.creedo.repositories;


public class DefaultRepositoryEntry<K,T> implements RepositoryEntry<K, T> {

	private K id;
	private T content;

	public DefaultRepositoryEntry(K id, T content) {
		this.id = id;
		this.content = content;
	}

	@Override
	public K getId() {
		return id;
	}

	@Override
	public T getContent() {
		return content;
	}

}
