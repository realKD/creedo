package de.unibonn.creedo.repositories;

import static de.unibonn.realkd.common.parameter.Parameters.rangeEnumerableParameter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//import com.google.common.base.Predicate;
//import com.google.common.collect.Iterables;
//import com.google.common.collect.Lists;

import java.util.function.Predicate;
import java.util.stream.Collectors;

import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;
import de.unibonn.realkd.common.base.Identifier;

/**
 * Range enumarable parameter that can take on all values that match identifiers
 * in a given repository.
 * 
 * @see {@link RangeEnumerableParameter}, {@link Repository}
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.0.1
 * 
 *          TODO: consolidate into Repositories
 *
 */
public class IdentifierInRepositoryParameter<T> extends DefaultRangeEnumerableParameter<String> {

	public static <T> RangeEnumerableParameter<String> getOptionalIdentifierInRepositoryParameter(Identifier id,
			String name, String description, Repository<String, T> repository,
			Predicate<RepositoryEntry<String, T>> filterPredicate) {
		return rangeEnumerableParameter(id, name, description, String.class, new RangeComputer<String>() {
			@Override
			public List<String> get() {
				List<String> result = new ArrayList<>();
				result.add("---");
				result.addAll(repository.getAllEntries(filterPredicate).stream().map(entry -> entry.getId())
						.collect(Collectors.toList()));
				return result;
			}
		});
	}

	public static <T> RangeEnumerableParameter<Optional<String>> optionalRepositoryIdParameter(Identifier id,
			String name, String description, Repository<String, T> repository,
			Predicate<RepositoryEntry<String, T>> filterPredicate) {
		return rangeEnumerableParameter(id, name, description, Optional.class, new RangeComputer<Optional<String>>() {
			@Override
			public List<Optional<String>> get() {
				List<Optional<String>> result = new ArrayList<>();
				result.add(Optional.empty());
				result.addAll(repository.getAllEntries(filterPredicate).stream()
						.map(entry -> Optional.of(entry.getId())).collect(Collectors.toList()));
				return result;
			}
		});
	}

	public IdentifierInRepositoryParameter(Identifier id, String name, String description,
			Repository<String, T> repository, Predicate<RepositoryEntry<String, T>> filterPredicate) {
		super(id, name, description, String.class, new RangeComputer<String>() {
			@Override
			public List<String> get() {
				return repository.getAllEntries(filterPredicate).stream().map(entry -> entry.getId())
						.collect(Collectors.toList());
			}
		});
	}

	public IdentifierInRepositoryParameter(Identifier id, String name, final String description,
			final Repository<String, T> repository) {
		this(id, name, description, repository, entry -> true);
	}

}
