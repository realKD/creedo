package de.unibonn.creedo.repositories;

import static de.unibonn.realkd.common.parameter.Parameters.subListParameter;
import static de.unibonn.realkd.common.parameter.Parameters.subSetParameter;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;
import de.unibonn.realkd.common.base.Identifier;

/**
 * <p>
 * Provides static methods for working with {@link Repository} objects. In
 * particular:
 * </p>
 * <ul>
 * <li>It allows to obtain predicates that can be used for filtering
 * repositories via {@link Repository#getAllEntries(Predicate)}. The predicates
 * returned by methods of this class might allow the actual repository
 * implementation to provide a more efficient filtering than the default
 * fallback of naively iterating through through all repository elements.
 * <li>It allows to obtain {@link Parameter} objects for parameters the valid
 * values of which are repository identifiers.
 * </ul>
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2.1
 *
 */
public class Repositories {

	public static final String[] REALKD_ENTITY_FILE_EXTENSIONS = { "jrke" };

	public static final String[] HTML_FILE_EXTENSIONS = { "htm", "html" };

	public static final String[] IMAGE_FILE_EXTENSIONS = { "jpg", "png", "gif", "bmp" };

	public static final String[] CSV_FILE_EXTENSIONS = { "csv", "txt" };

	public static final String[] ARF_FILE_EXTENSIONS = { "arff", "arf", "xarf" };

	public static final String[] JS_FILE_EXTENSIONS = { "js" };

	public static <T> Predicate<RepositoryEntry<String, T>> getIdMatchingRegexPredicate(final Pattern regexPattern) {
		return new Predicate<RepositoryEntry<String, T>>() {

			@Override
			public boolean test(RepositoryEntry<String, T> entry) {
				return regexPattern.matcher(entry.getId()).matches();
			}
		};
	}

	public static <T> Predicate<RepositoryEntry<String, T>> getIdIsFilenameWithExtensionPredicate(
			String... extensions) {
		StringBuilder extensionPart = new StringBuilder();
		Iterator<String> extIterator = Arrays.asList(extensions).iterator();
		while (extIterator.hasNext()) {
			extensionPart.append(extIterator.next());
			if (extIterator.hasNext()) {
				extensionPart.append("|");
			}
		}
		Pattern pattern = Pattern.compile("([^\\s]+(\\.(?i)(" + extensionPart.toString() + "))$)");

		return getIdMatchingRegexPredicate(pattern);
	}

	public static <T> SubCollectionParameter<String, List<String>> getIdentifierListOfRepositoryParameter(Identifier id,
			String name, String description, Repository<String, T> repository) {
		return getIdentifierListOfRepositoryParameter(id, name, description, repository, entry -> true);
	}

	public static <T> SubCollectionParameter<String, List<String>> getIdentifierListOfRepositoryParameter(Identifier id,
			String name, final String description, Repository<String, T> repository,
			Predicate<RepositoryEntry<String, T>> filterPredicate) {
		Supplier<List<String>> collectionComputer = () -> repository.getAllEntries(filterPredicate).stream()
				.map(entry -> entry.getId()).collect(Collectors.toList());
		return subListParameter(id, name, description, collectionComputer);
	}

	public static <T> SubCollectionParameter<String, Set<String>> getIdentifierSetOfRepositoryParameter(Identifier id,
			String name, String description, Repository<String, T> repository) {
		return Repositories.getIdentifierSetOfRepositoryParameter(id, name, description, repository, entry -> true);
	}

	public static <T> SubCollectionParameter<String, Set<String>> getIdentifierSetOfRepositoryParameter(Identifier id,
			String name, final String description,  Repository<String, T> repository,
			Predicate<RepositoryEntry<String, T>> filterPredicate) {
		Supplier<Set<String>> collectionComputer = () -> repository.getAllEntries(filterPredicate).stream()
				.map(entry -> entry.getId()).collect(Collectors.toSet());
		return subSetParameter(id, name, description, collectionComputer);
	}

}
