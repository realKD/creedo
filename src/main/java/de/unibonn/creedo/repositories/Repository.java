package de.unibonn.creedo.repositories;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Abstract interface for an indexed collection of generic type. Specific
 * implementations have specific conditions as to what objects can be correctly
 * stored in the collection.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2.1
 *
 * @param <T>
 *            the type of the repository values
 * 
 * @param <K>
 *            the type of the repository keys
 * 
 */
public interface Repository<K, T> {

	public String getName();

	public void add(K id, T content);

	public void delete(K id);

	public void update(RepositoryEntry<K, T> entry);

	public List<K> getAllIds();

	public RepositoryEntry<K, T> getEntry(K id);

	public Collection<RepositoryEntry<K, T>> getAllEntries();
	
	public default Optional<T> getOpt(K id) {
		if (getAllIds().contains(id)) {
			return Optional.of(get(id));
		}
		else {
			return Optional.empty();
		}
	}

	public default T get(K id) {
		return getEntry(id).getContent();
	}

	public default Collection<RepositoryEntry<K, T>> getAllEntries(
			Predicate<RepositoryEntry<K, T>> predicate) {
		Collection<RepositoryEntry<K, T>> all = this.getAllEntries();
		return all.stream().filter(predicate).collect(Collectors.toList());
	}

	public default Collection<T> getAll() {
		return getAllEntries().stream().map(e -> e.getContent())
				.collect(Collectors.toList());
	}

	public default Collection<T> getAll(
			Predicate<RepositoryEntry<K, T>> predicate) {
		return getAllEntries(predicate).stream().map(e -> e.getContent())
				.collect(Collectors.toList());
	}

	public default void clear() {
		getAllIds().forEach(id -> {
			delete(id);
		});
	}

}
