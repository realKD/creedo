package de.unibonn.creedo.repositories.mybatisimpl;

public class ParameterTableRow {

    private String parameterName;
    private String parameterValue;

    public ParameterTableRow() {
    }

    public String getParameterName() {
        return parameterName;
    }

    public String getParameterValue() {
        return parameterValue;
    }
	
}
