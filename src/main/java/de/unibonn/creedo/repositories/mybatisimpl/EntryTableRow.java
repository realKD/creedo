package de.unibonn.creedo.repositories.mybatisimpl;

public class EntryTableRow {
	
	private String id;
		
	private String contentClassName;
	
	private EntryTableRow() {
		;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContentClassName() {
		return contentClassName;
	}


}
