package de.unibonn.creedo.studies.state.mybatisimpl;

import java.util.Date;

/**
 * Used by {@link OldCreedoStudyStateMapper} to map results of sql queries to
 * the trial table.
 * 
 * @author Mario Boley
 *
 * @since 0.1.2
 * 
 * @version 0.1.2.1
 * 
 */
public class MyBatisTrialSessionQueryResult {

	private Integer sessionId;

	private String userId;

	private String taskName;

	private String systemName;

	private Date endTime;

	public Integer getSessionId() {
		return sessionId;
	}

	public void setSessionId(Integer sessionId) {
		this.sessionId = sessionId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	// @Override
	// public TrialSession get() {
	// return new TrialSession(sessionId, new UserDAO().getUserById(userId),
	// system, task, endDate);
	// }

}
