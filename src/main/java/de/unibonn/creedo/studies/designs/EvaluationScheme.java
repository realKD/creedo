package de.unibonn.creedo.studies.designs;

import static de.unibonn.realkd.common.parameter.Parameters.stringParameter;
import static de.unibonn.realkd.common.base.Identifier.id;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.creedo.setup.ServerPaths;
import de.unibonn.creedo.webapp.studies.rating.RatingMetric;
import de.unibonn.realkd.common.parameter.DefaultParameterContainer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.Parameters;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;
import de.unibonn.realkd.util.Predicates;

public class EvaluationScheme implements ParameterContainer {

	private final Parameter<String> nameParameter;

	private final Parameter<String> descriptionParameter;

	private final SubCollectionParameter<String, List<String>> instructionParameter;

	private final SubCollectionParameter<RatingMetric, Set<RatingMetric>> ratingMetricParameter;

	private final DefaultParameterContainer defaultParameterContainer;

	public EvaluationScheme() {

		nameParameter = stringParameter(id("name"), "Name", "The name of this evaluation scheme.", "",
				Predicates.notNull(), "");

		descriptionParameter = stringParameter(id("description"), "Description",
				"A descripton that explains this evalaution scheme.", "", Predicates.notNull(), "");

		instructionParameter = Repositories.getIdentifierListOfRepositoryParameter(id("Instructions"), "Instructions",
				"Select a list of instruction pages for the corresponding evaluation task.",
				ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
				Repositories.getIdIsFilenameWithExtensionPredicate(Repositories.HTML_FILE_EXTENSIONS));

		ratingMetricParameter = Parameters.subSetParameter(id("Rating_Metrics"), "Rating Metrics",
				"Select a set of rating metrics that evaluators will use to rate the reuslts from trial tasks.",
				() -> Sets.newHashSet(RatingMetric.values()));

		defaultParameterContainer = new DefaultParameterContainer();

		defaultParameterContainer.addAllParameters(Arrays.asList((Parameter<?>) nameParameter, descriptionParameter,
				instructionParameter, ratingMetricParameter));

	}

	public List<String> getInstructions() {
		List<String> instructions = new ArrayList<>();
		for (String fileName : instructionParameter.current()) {
			instructions.add(ServerPaths.contentFileContentAsString(fileName));
		}
		return instructions;
	}

	public List<RatingMetric> getRatingMetrics() {
		return new ArrayList<>(ratingMetricParameter.current());

	}

	public String getName() {
		return nameParameter.current();
	}

	public String getDescription() {
		return descriptionParameter.current();
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return defaultParameterContainer.getTopLevelParameters();
	}

}
