$(document).ready(function() {

var title_1 = "Welcome to the SubGroup Discovery (SGD) tutorial!";
var content_1 = "Contents: <br><br>1. Presentation of the data on the octet-binaries materials <br>2. Running the SGD algorithm <br> 3. Analysis and visualization of the results ";

var title_2 = "Tutorial tour";
var content_2 = "If you need more information click <b>HELP</b> in the upper right. If you are done with the tutorial click <b>CLOSE</b>.";


var title_3 = "Octet-binaries data";
var content_3 = "This table contains data on 82 octet binary compounds (materials with eight valence electrons in the unit cell) and their atomic constituents, retrieved from the NOMAD Archive.";

var title_4 = "Octet-binaries data";
var content_4 = "The first three columns show the target variables while the remaining columns show the features. In the column 'Delta E', for each compound the energy difference between its rocksalt and zincblende phase (both in mechanical equilibrium) is listed. Based on these numbers, the two next columns 'sign(Delta E)' and 'three class Delta E' classify the compound into RS / ZB and RS / Delta E &#126; 0 / ZB, respectively. Move the cursor to the table headers for displaying information on the data. To check each compound (one per row), you can scroll or switch to the next section of the table by clicking at its bottom right.";

var title_5 = "Octet-binaries data";
var content_5 = "The next ten columns contain the <i>primary features</i>, i.e., properties of the atomic constituents of the AB compounds, followed by <i>derived features</i>, i.e., algebraic combinations of the primary features. The last three columns contain combinations which were identified by the LASSO+<i>l</i><sub>0</sub> method <a target='_blank' href='http://journals.aps.org/prl/abstract/10.1103/PhysRevLett.114.105503'>(Click here for the free access PDF) </a> to describe the energy differences accurately in a linear combination.";

var title_6 = "Run the algorithm";
var content_6 = "To run the SGD algorithm click <b>Mine</b> and specify the parameters. (the tutorial tour will continue after clicking <b>Mine</b>)";

var title_7 = "Algorithm parameters";
var content_7 = "First choose a <b>Target variable</b>!<br><br> With the <b>Attribute filter</b> you can exclude features from the data table.<br><br>Specify your <b>Objective function</b>, e.g. how strong should the contribution of the subgroup size (frequency) be weighted. <br><br> Choose the <b>Number of seeds</b>. The higher this number the higher the probability to find all the relevant subgroups. <br><br>Click <b>Execute!</b>";

var title_7a = "";
var content_7a = "The calculations are running, now. Please wait some seconds!";

var title_8 = "Results";
var content_8 = "The different subgroups found by the SGD algorithm are shown in the small windows on the right and are ranked by the target-function score. The subgroup is represented by the color red and defined by the inequalities in the top of the window. The blue color represents the whole data.";

var title_9 = "Analysis";
var content_9 = "Now go to <b>Scatter plot</b>.";

var title_10 = "Analysis";
var content_10 = "In the plot, the data points represent the 82 octet binary compounds, where the <i>x</i> and <i>y</i> coordinates are the features selected by the LASSO+<i>l</i><sub>0</sub> algorithm as the components of the two-dimensional optimal descriptor for the property Delta E (Check the tutorial 'Predicting the energy difference between different crystal structures' <a target='_blank' href='https://labdev-nomad.esc.rzg.mpg.de/home/'>here</a> ).";

var title_11 = "Analysis";
var content_11 = "Let us now visualize the subgroups found by the SGD algorithm. Drag and drop a subgroup window from the list on the right to the dashboard below. Give the subgroup a name by typing in the lower left of the window and confirm with [Enter]. Then click the cross-hairs symbol in the upper right of the window to visualize the subgroup distribution on the map.";

var title_12 = "Analysis";
var content_12 = "You can change the axes by clicking one of the two labels. Choose the features which define the subgroup in the upper left of the subgroup window to be the new axes and compare the inequalities with the distribution of the subgroup on the plot.";


var title_13 = "Analysis";
var content_13 = "You can pull further subgroup windows down and visualize their distribution on the plot. To deactivate the markings of a subgroup in the plot click again the cross-hairs symbol.";

var title_14 = "End";
var content_14 = "Congratulations! You have finished the SGD tutorial.";










//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////////////////////////    script    ////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
	console.log("Loading tour for octet binary tutorial");

// templates for different tir steps

stan = '<div class="popover" role="tooltip"> <div class="arrow"></div> <h3 class="popover-title"></h3> <div class="popover-content"></div> <div class="popover-navigation"> <div class="btn-group"> <button class="btn btn-sm btn-default" data-role="prev">&laquo; Prev</button> <button class="btn btn-sm btn-default" data-role="next">Next &raquo;</button> <button class="btn btn-sm btn-default" data-role="pause-resume" data-pause-text="Pause" data-resume-text="Resume">Pause</button> </div> <button class="btn btn-sm btn-default" data-role="end">End tour</button> </div> </div>' //standard
no_arrow = '<div class="popover" role="tooltip">  <h3 class="popover-title"></h3> <div class="popover-content"></div> <div class="popover-navigation"> <div class="btn-group"> <button class="btn btn-sm btn-default" data-role="prev">&laquo; Prev</button> <button class="btn btn-sm btn-default" data-role="next">Next &raquo;</button> <button class="btn btn-sm btn-default" data-role="pause-resume" data-pause-text="Pause" data-resume-text="Resume">Pause</button> </div> <button class="btn btn-sm btn-default" data-role="end">End tour</button> </div> </div>' // window without arrow
no_next = '<div class="popover" role="tooltip"> <div class="arrow"></div> <h3 class="popover-title"></h3> <div class="popover-content"></div> <div class="popover-navigation"> <div class="btn-group"> <button class="btn btn-sm btn-default" data-role="prev">&laquo; Prev</button> <button class="btn btn-sm btn-default disabled" data-role="next">Next &raquo;</button> <button class="btn btn-sm btn-default" data-role="pause-resume" data-pause-text="Pause" data-resume-text="Resume">Pause</button> </div> <button class="btn btn-sm btn-default" data-role="end">End tour</button> </div> </div>' //next button disabled
no_arrownextprev = '<div class="popover" role="tooltip">  <h3 class="popover-title"></h3> <div class="popover-content"></div> <div class="popover-navigation"> <div class="btn-group"> <button class="btn btn-sm btn-default disabled" data-role="prev">&laquo; Prev</button> <button class="btn btn-sm btn-default disabled" data-role="next">Next &raquo;</button> <button class="btn btn-sm btn-default" data-role="pause-resume" data-pause-text="Pause" data-resume-text="Resume">Pause</button> </div> <button class="btn btn-sm btn-default" data-role="end">End tour</button> </div> </div>' //prev and next button disabled and no arrow
no_prev = '<div class="popover" role="tooltip"> <div class="arrow"></div> <h3 class="popover-title"></h3> <div class="popover-content"></div> <div class="popover-navigation"> <div class="btn-group"> <button class="btn btn-sm btn-default disabled" data-role="prev">&laquo; Prev</button> <button class="btn btn-sm btn-default" data-role="next">Next &raquo;</button> <button class="btn btn-sm btn-default" data-role="pause-resume" data-pause-text="Pause" data-resume-text="Resume">Pause</button> </div> <button class="btn btn-sm btn-default" data-role="end">End tour</button> </div> </div>' //prev button disabled

//Tour object
var tour = new Tour({
  storage: false,
  steps: [
  {   
    element: "#analysis-patterns",
    title: title_1,
    content: content_1,
    placement: "top",
    //backdrop:true,
    //backdropContainer: 'body',
    template:no_arrow
  },  
  {   
    element: "#analysis-patterns",
    title: title_2,
    content: content_2,
    placement: "top",
    template:no_arrow
  },  
  {   
    element: "#content-table",
    title: title_3,
    content: content_3,
    placement: "right",
    template:stan
  },  
  {   
    element: "#content-table",
    title: title_4,
    content: content_4,
    placement: "right",
    template:stan
  },  

  {   
    element: "#content-table",
    title: title_5,
    content: content_5,
    placement: "right",
    template:stan
  },  
  {   
    element: "#btn-start-mining",
    title: title_6,
    content: content_6,
    placement: "bottom",
    onShown:function (tour) { ask_div('refParametersArea',6) },
    template: no_next
  },  
   {   
    element: "#refParametersArea",
    title: title_7,
    content: content_7,
    placement: "right",
    template: no_arrownextprev
   },  
   {   
    element: "#refParametersArea",
    title: title_7a,
    content: content_7a,
    placement: "right",
    template: no_arrownextprev
   },  
   {   
    element: "#candidate-patterns",
    title: title_8,
    content: content_8,
    placement: "left",
    template: no_prev
   },   
   {   
    element: "#tab-point-cloud",
    title: title_9,
    content: content_9,
    placement: "bottom",
    onShown:function (tour) { ask_div('content-point-cloud',10); },
    template: no_next
   },   
   {   
    element: "#content-point-cloud",
    title: title_10,
    content: content_10,
    placement: "right",
    template: no_prev
   },   
   {   
    element: "#analysis-patterns",
    title: title_11,
    content: content_11,
    placement: "top",
    template: stan 
   },   
   {   
    element: "#content-point-cloud",
    title: title_12,
    content: content_12,
    placement: "right",
    template: stan 
   },   
   {   
    element: "#analysis-patterns",
    title: title_13,
    content: content_13,
    placement: "top",
    template: stan 
   },   
   {   
    element: "#analysis-patterns",
    title: title_14,
    content: content_14,
    placement: "top",
    template: no_arrow
   }    

]});



$("#refExecuteButton").click(function(){
  tour.goTo(7)
  
})

function ask_div(div_id,step_i,div_class){ // next/step_i'th tour element if div_id is visible. If div_class argument is specified only next/step_i'th element if class_name matches. If step_i is passed as arguement then step_i'th element, if undefined next element 
  if (typeof(step_i)==='undefined') {step_i =-1;} 
  div_id = '#' + div_id;
  var intervall = setInterval(function() {
    if (typeof(div_class)==='undefined') {if_class =true;} 
    else {
      if_class = $( div_id ).hasClass( div_class );
    }
    if($(div_id).is(':visible') && if_class) {
      clearInterval(intervall);
      if (step_i==-1){
        tour.next();
      } else {
      tour.goTo(step_i);
      }
    };
  }, 800);
}

function disabled_enabled(div_id,step_i){ //next tour element if class name was first "disabled" and then "". 
  if (typeof(step_i)==='undefined') {step_i =-1;} 
  var botton = 0;
  var intervall = setInterval(function() {
    class_name =  document.getElementById(div_id).className;
    if (class_name == "disabled") {button = 1;}
    if (button == 1 && class_name == "") {
      clearInterval(intervall);
      if (step_i==-1){
        tour.next();
      } else {
      tour.goTo(step_i);
      }

    }
  }, 800);
}

$('[data-toggle="popover"]').popover({title: "Header", content: "Text in popover body",});
document.getElementById('tab-content').click();
disabled_enabled('candidate-patterns',7);
tour.init();
tour.start();
});

