$(document).ready(function() {

var title_1 = "Welcome to the SubGroup Discovery (SGD) tutorial!";
var content_1 = "Contents: <br><br>1. Presentation of the data on the gold clusters (having sizes of 5-14 atoms) <br>2. Running the SGD algorithm <br> 3. Analysis and visualization of the results ";

var title_2 = "Tutorial tour";
var content_2 = "If you need more information click on <b>HELP</b> in the upper right. If you are done with the tutorial please click on <b>CLOSE</b>.";

var title_3 = "Gold clusters data";
var content_3 = "This table contains data on 12,220 gold cluster configurations (1,222 configurations per size, of sizes 5-14 atoms) obtained from ab initio molecular dynamics.";

var title_4 = "Gold clusters data";
var content_4 = "The columns contain data pertaining to the target variables and the descriptive features. Move the cursor ontop of the table headers to display information about the features. To check each gold cluster configuration's data (one per row), you can scroll or switch to the next section of the table by clicking at the bottom right.";

var title_5 = "Gold clusters data";
var content_5 = "All of the descriptive features were obtained from electronic-structure calculations of the gold clusters and their geometry, e.g., ionization potential, electron affinity, HOMO-LUMO energy gap, normalized radius of gyration."

var title_6 = "Run the algorithm";
var content_6 = "To run the SGD algorithm click on the <b>Mine</b> button and specify the parameters (the tutorial tour will continue after clicking <b>Mine</b>).";

var title_7 = "Algorithm parameters";
var content_7 = "First choose a <b>Target variable</b>!<br><br> with the <b>Attribute filter</b> you can exclude features from the data table.<br><br>Specify your <b>Objective function</b>, e.g. how much should the contribution of the subgroup size (frequency) be weighted. <br><br> Choose the <b>Number of seeds</b>. The higher this number the higher the probability to find all the relevant subgroups. <br><br> Click on <b>Execute!</b>";

var title_7a = "";
var content_7a = "The calculations are running, now. Please wait some minutes!";

var title_8 = "Results";
var content_8 = "The different subgroups found by the SD algorithm are shown in the small windows on the right and are ranked by the quality (objective function) score. The subgroup is represented by the color red and defined by the statement (logical conjunctions of descriptive features) in the top of the window. The blue color represents the global data.";

var title_9 = "Analysis";
var content_9 = "Now go to 'Scatter plot'.";

var title_10 = "Analysis";
var content_10 = "In the plot, the data points represent the 12 220 gold cluster configurations, where the default <i>y</i> and <i>x</i> coordinates are the temperature at which the gold cluster configurations were generated and their relative energy.";

var title_11 = "Analysis";
var content_11 = "Let us now visualize the subgroups found by the SGD algorithm. Drag and drop a subgroup window from the list on the right to the dashboard below. Give the subgroup a name by typing in the lower left of the window and confirm with [Enter]. Then click on the cross-hairs symbol in the upper right of the window to visualize the subgroup distribution on the map.";

var title_12 = "Analysis";
var content_12 = "You can change the axes by clicking on one of the two labels. Choose the features which define the subgroup in the upper left of the subgroup window to be the new axes and compare the inequalities with the distribution of the subgroup on the plot.";

var title_13 = "Analysis";
var content_13 = "You can pull further subgroup windows down and visualize their distribution on the plot. To deactivate the markings of a subgroup in the plot click again on the cross-hairs symbol.";

var title_14 = "End";
var content_14 = "Congratulations! You have finished the SGD tutorial.";

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////////////////////////    script    ////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
	console.log("Loading tour for octet binary tutorial");

// templates for different tir steps

stan = '<div class="popover" role="tooltip"> <div class="arrow"></div> <h3 class="popover-title"></h3> <div class="popover-content"></div> <div class="popover-navigation"> <div class="btn-group"> <button class="btn btn-sm btn-default" data-role="prev">&laquo; Prev</button> <button class="btn btn-sm btn-default" data-role="next">Next &raquo;</button> <button class="btn btn-sm btn-default" data-role="pause-resume" data-pause-text="Pause" data-resume-text="Resume">Pause</button> </div> <button class="btn btn-sm btn-default" data-role="end">End tours</button> </div> </div>' //standard
no_arrow = '<div class="popover" role="tooltip">  <h3 class="popover-title"></h3> <div class="popover-content"></div> <div class="popover-navigation"> <div class="btn-group"> <button class="btn btn-sm btn-default" data-role="prev">&laquo; Prev</button> <button class="btn btn-sm btn-default" data-role="next">Next &raquo;</button> <button class="btn btn-sm btn-default" data-role="pause-resume" data-pause-text="Pause" data-resume-text="Resume">Pause</button> </div> <button class="btn btn-sm btn-default" data-role="end">End tours</button> </div> </div>' // window without arrow
no_next = '<div class="popover" role="tooltip"> <div class="arrow"></div> <h3 class="popover-title"></h3> <div class="popover-content"></div> <div class="popover-navigation"> <div class="btn-group"> <button class="btn btn-sm btn-default" data-role="prev">&laquo; Prev</button> <button class="btn btn-sm btn-default disabled" data-role="next">Next &raquo;</button> <button class="btn btn-sm btn-default" data-role="pause-resume" data-pause-text="Pause" data-resume-text="Resume">Pause</button> </div> <button class="btn btn-sm btn-default" data-role="end">End tours</button> </div> </div>' //next button disabled
no_arrownextprev = '<div class="popover" role="tooltip">  <h3 class="popover-title"></h3> <div class="popover-content"></div> <div class="popover-navigation"> <div class="btn-group"> <button class="btn btn-sm btn-default disabled" data-role="prev">&laquo; Prev</button> <button class="btn btn-sm btn-default disabled" data-role="next">Next &raquo;</button> <button class="btn btn-sm btn-default" data-role="pause-resume" data-pause-text="Pause" data-resume-text="Resume">Pause</button> </div> <button class="btn btn-sm btn-default" data-role="end">End tours</button> </div> </div>' //prev and next button disabled and no arrow
no_prev = '<div class="popover" role="tooltip"> <div class="arrow"></div> <h3 class="popover-title"></h3> <div class="popover-content"></div> <div class="popover-navigation"> <div class="btn-group"> <button class="btn btn-sm btn-default disabled" data-role="prev">&laquo; Prev</button> <button class="btn btn-sm btn-default" data-role="next">Next &raquo;</button> <button class="btn btn-sm btn-default" data-role="pause-resume" data-pause-text="Pause" data-resume-text="Resume">Pause</button> </div> <button class="btn btn-sm btn-default" data-role="end">End tours</button> </div> </div>' //prev button disabled

//Tour object
var tour = new Tour({
  storage: false,
  steps: [
  {   
    element: "#analysis-patterns",
    title: title_1,
    content: content_1,
    placement: "top",
    //backdrop:true,
    //backdropContainer: 'body',
    template:no_arrow
  },  
  {   
    element: "#analysis-patterns",
    title: title_2,
    content: content_2,
    placement: "top",
    template:no_arrow
  },  
  {   
    element: "#content-table",
    title: title_3,
    content: content_3,
    placement: "right",
    template:stan
  },  
  {   
    element: "#content-table",
    title: title_4,
    content: content_4,
    placement: "right",
    template:stan
  },  

  {   
    element: "#content-table",
    title: title_5,
    content: content_5,
    placement: "right",
    template:stan
  },  
  {   
    element: "#btn-start-mining",
    title: title_6,
    content: content_6,
    placement: "bottom",
    onShown:function (tour) { ask_div('refParametersArea',6) },
    template: no_next
  },  
   {   
    element: "#refParametersArea",
    title: title_7,
    content: content_7,
    placement: "right",
    template: no_arrownextprev
   },  
   {   
    element: "#refParametersArea",
    title: title_7a,
    content: content_7a,
    placement: "right",
    template: no_arrownextprev
   },  
   {   
    element: "#candidate-patterns",
    title: title_8,
    content: content_8,
    placement: "left",
    template: no_prev
   },   
   {   
    element: "#tab-point-cloud",
    title: title_9,
    content: content_9,
    placement: "bottom",
    onShown:function (tour) { ask_div('content-point-cloud',10); },
    template: no_next
   },   
   {   
    element: "#content-point-cloud",
    title: title_10,
    content: content_10,
    placement: "right",
    template: no_prev
   },   
   {   
    element: "#analysis-patterns",
    title: title_11,
    content: content_11,
    placement: "top",
    template: stan 
   },   
   {   
    element: "#content-point-cloud",
    title: title_12,
    content: content_12,
    placement: "top",
    template: stan 
   },   
   {   
    element: "#analysis-patterns",
    title: title_13,
    content: content_13,
    placement: "top",
    template: stan 
   },   
   {   
    element: "#analysis-patterns",
    title: title_14,
    content: content_14,
    placement: "top",
    template: no_arrow
   }    

]});



$("#refExecuteButton").click(function(){
  tour.goTo(7)
  
})

function ask_div(div_id,step_i,div_class){ // next/step_i'th tour element if div_id is visible. If div_class argument is specified only next/step_i'th element if class_name matches. If step_i is passed as arguement then step_i'th element, if undefined next element 
  if (typeof(step_i)==='undefined') {step_i =-1;} 
  div_id = '#' + div_id;
  var intervall = setInterval(function() {
    if (typeof(div_class)==='undefined') {if_class =true;} 
    else {
      if_class = $( div_id ).hasClass( div_class );
    }
    if($(div_id).is(':visible') && if_class) {
      clearInterval(intervall);
      if (step_i==-1){
        tour.next();
      } else {
      tour.goTo(step_i);
      }
    };
  }, 800);
}

function disabled_enabled(div_id,step_i){ //next tour element if class name was first "disabled" and then "". 
  if (typeof(step_i)==='undefined') {step_i =-1;} 
  var botton = 0;
  var intervall = setInterval(function() {
    class_name =  document.getElementById(div_id).className;
    if (class_name == "disabled") {button = 1;}
    if (button == 1 && class_name == "") {
      clearInterval(intervall);
      if (step_i==-1){
        tour.next();
      } else {
      tour.goTo(step_i);
      }

    }
  }, 800);
}


document.getElementById('tab-content').click();
disabled_enabled('candidate-patterns',7);
tour.init();
tour.start();
});

