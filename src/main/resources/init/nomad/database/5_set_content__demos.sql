INSERT INTO `content__demos` VALUES ('octed_binaries_tutorial', 'de.unibonn.creedo.ui.indexpage.DashboardLinkBuilder');
INSERT INTO `content__demos_parameters` (`entry_id`, `param_name`, `param_value`) VALUES
  ('octed_binaries_tutorial', 'Title', 'Octet Binary Crystal Structure Characterization'),
  ('octed_binaries_tutorial', 'Description', 'Use subgroup discovery to find descriptors for predicting the formation of rocksalt or zinkblende crystal structures.'),
  ('octed_binaries_tutorial', 'Image', 'octed_binaries_subgroup.png'),
  ('octed_binaries_tutorial', 'Image credits', 'Image: Ghiringhelli et al., 2015'),
  ('octed_binaries_tutorial', 'User groups', '[REGISTERED, ANONYMOUS]'),
  ('octed_binaries_tutorial', 'Analytics dashboard', 'nomad_sgd_binaries_tutorial_system'),
  ('octed_binaries_tutorial', 'Data', 'binaries_tutorial');
 
INSERT INTO `content__demos` VALUES ('gold_tutorial', 'de.unibonn.creedo.ui.indexpage.DashboardLinkBuilder');
INSERT INTO `content__demos_parameters` (`entry_id`, `param_name`, `param_value`) VALUES
  ('gold_tutorial', 'Title', 'Gold Structure-Property Relationships'),
  ('gold_tutorial', 'Description', 'Use subgroup discovery to uncover different structure-property relationships of gold nano clusters.'),
  ('gold_tutorial', 'Image', 'image_all_gold_structures.png'),
  ('gold_tutorial', 'Image credits', 'Image: B. R. Goldsmith et al.'),
  ('gold_tutorial', 'User groups', '[REGISTERED, ANONYMOUS]'),
  ('gold_tutorial', 'Analytics dashboard', 'nomad_gold_tutorial_system'),
  ('gold_tutorial', 'Data', 'gold_clusters');
