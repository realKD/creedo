INSERT INTO `mining__data` (`id`,`content_class_name`) VALUES ('binaries_tutorial', 'de.unibonn.creedo.boot.XarfBasedWorkspaceBuilder');
INSERT INTO `mining__data_parameters` (`entry_id`,`param_name`,`param_value`) VALUES ('binaries_tutorial', 'Data file', 'data_octet_binaries_tutorial.xarf');
INSERT INTO `mining__data_parameters` (`entry_id`,`param_name`,`param_value`) VALUES ('binaries_tutorial', 'Group mappers', '[CONSECUTIVE_CHANGE_ATTRIBUTES, DISTRIBUTION_MODE, DISTRIBUTION_SHAPE, DISTRIBUTION_MEDIAN]');
INSERT INTO `mining__data_parameters` (`entry_id`,`param_name`,`param_value`) VALUES ('binaries_tutorial', 'Attribute mappers', '[CATEGORIC_EQUALiTY, SMART_DISCRETE_ORDINAL, POSITIVE_AND_NEGATIVE, YEAR_MONTH_DATE_HOUR, CLUSTERING_18_CUTOFFS]');
INSERT INTO `mining__data_parameters` (`entry_id`,`param_name`,`param_value`) VALUES ('binaries_tutorial', 'Additional entities', '[data_octet_binaries_tutorial_scatter_configuration.jrke]');

INSERT INTO `mining__data` (`id`,`content_class_name`) VALUES ('gold_clusters', 'de.unibonn.creedo.boot.XarfBasedWorkspaceBuilder');
INSERT INTO `mining__data_parameters` (`entry_id`,`param_name`,`param_value`) VALUES ('gold_clusters', 'Data file', 'data_gold_stratsample12200_2.0.1.xarf');
INSERT INTO `mining__data_parameters` (`entry_id`,`param_name`,`param_value`) VALUES ('gold_clusters', 'Group mappers', '[CONSECUTIVE_CHANGE_ATTRIBUTES, DISTRIBUTION_MODE, DISTRIBUTION_SHAPE, DISTRIBUTION_MEDIAN]');
INSERT INTO `mining__data_parameters` (`entry_id`,`param_name`,`param_value`) VALUES ('gold_clusters', 'Attribute mappers', '[CATEGORIC_EQUALiTY, SMART_DISCRETE_ORDINAL, POSITIVE_AND_NEGATIVE, IRREGULAR_4_CUTOFFS_CLUSTERING, EVEN_ODD]');
