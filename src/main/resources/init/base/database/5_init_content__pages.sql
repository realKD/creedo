CREATE TABLE `content__pages`
(
  `id`                  VARCHAR(255) PRIMARY KEY NOT NULL,
  `content_class_name`  TEXT
);

CREATE TABLE `content__pages_parameters` (
  `id`          INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `entry_id`  VARCHAR(255),
  `param_name`  TEXT,
  `param_value` TEXT,
  FOREIGN KEY (`entry_id`) REFERENCES `content__pages` (`id`)
);
